package com.shanthi.application;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

public class SandApp extends Application {
    private static SandApp dairyFirmApp;

    @Override
    public void onCreate() {
        super.onCreate();
        dairyFirmApp = this;
    }

    public static SandApp getInstance() {
        return dairyFirmApp;
    }

    public static Context getStaticContext() {
        return SandApp.getInstance().getApplicationContext();
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
       MultiDex.install(this);
    }
}
