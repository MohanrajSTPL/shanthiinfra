package com.shanthi.network.response.enquiry;

import com.shanthi.model.enquiry.Frequency;
import com.shanthi.network.response.HeaderResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FrequencyResponse extends HeaderResponse {
    @SerializedName("resp")
    @Expose
    private List<Frequency> frequencyList = null;

    public void setFrequencyList(List<Frequency> frequencyList) {
        this.frequencyList = frequencyList;
    }

    public List<Frequency> getFrequencyList() {
        return frequencyList;
    }
}