package com.shanthi.network.response.order;

import com.shanthi.model.order.Uom;
import com.shanthi.network.response.HeaderResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UomResponse extends HeaderResponse {

    @SerializedName("resp")
    @Expose
    private List<Uom> uomList = null;

    public List<Uom> getUomList() {
        return uomList;
    }

    public void setUomList(List<Uom> uomList) {
        this.uomList = uomList;
    }
}