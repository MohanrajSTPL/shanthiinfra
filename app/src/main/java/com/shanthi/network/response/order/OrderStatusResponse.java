package com.shanthi.network.response.order;

import com.shanthi.model.order.Status;
import com.shanthi.network.response.HeaderResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderStatusResponse extends HeaderResponse {
    @SerializedName("resp")
    @Expose

    private List<Status> statusList = null;

    public List<Status> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<Status> statusList) {
        this.statusList = statusList;
    }
}


