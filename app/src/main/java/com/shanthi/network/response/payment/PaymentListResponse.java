package com.shanthi.network.response.payment;

import com.shanthi.model.payment.Payments;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaymentListResponse {
    @SerializedName("Payments")
    @Expose
    private List<Payments> payments = null;
    @SerializedName("Response_code")
    @Expose
    private String responseCode;
    @SerializedName("Message")
    @Expose
    private String message;

    public List<Payments> getPayments() {
        return payments;
    }

    public void setPayments(List<Payments> payments) {
        this.payments = payments;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
