package com.shanthi.network.response.List;

import com.shanthi.model.List.OrdersList;
import com.shanthi.network.response.HeaderResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewOrderListResponse extends HeaderResponse {

    @SerializedName("orders")
    @Expose

    private List<OrdersList> ordersLists = null;

    public List<OrdersList> getOrders() {
        return ordersLists;
    }

    public void setOrders(List<OrdersList> ordersLists) {
        this.ordersLists = ordersLists;
    }
}
