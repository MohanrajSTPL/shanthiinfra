package com.shanthi.network.response.enquiry;

import com.shanthi.model.enquiry.Enquiry;
import com.shanthi.network.response.HeaderResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EnquiryResponse extends HeaderResponse {
    @SerializedName("resp")
    @Expose
    private List<Enquiry> enquiryList = null;

    public List<Enquiry> getEnquiryList() {
        return enquiryList;
    }

    public void setEnquiryList(List<Enquiry> enquiryList) {
        this.enquiryList = enquiryList;
    }
}