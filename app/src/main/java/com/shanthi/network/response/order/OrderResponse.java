package com.shanthi.network.response.order;

import com.shanthi.model.order.Order;
import com.shanthi.network.response.HeaderResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderResponse extends HeaderResponse {
    @SerializedName("resp")
    @Expose
    private List<Order> orderList = null;

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }

    public List<Order> getOrderList() {
        return orderList;
    }
}