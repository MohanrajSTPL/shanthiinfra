package com.shanthi.network.response.order;

import com.shanthi.model.order.OrderItem;
import com.shanthi.network.response.HeaderResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderItemResponse extends HeaderResponse {
    @SerializedName("resp")
    @Expose
    private List<OrderItem> orderItemList = null;

    public List<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(List<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }
}