package com.shanthi.network.response.expenseType;

import com.shanthi.model.construction.Approval;
import com.shanthi.model.shanthiInfra.Payment;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApprovalResponse {

    @SerializedName("Approvals")
    @Expose
    private List<Approval> approvals = null;
    @SerializedName("Payments")
    @Expose
    private List<Payment> payments = null;
    @SerializedName("Response_code")
    @Expose
    private String responseCode;
    @SerializedName("Message")
    @Expose
    private String message;


    public List<Approval> getApprovals() {
        return approvals;
    }

    public void setApprovals(List<Approval> approvals) {
        this.approvals = approvals;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
