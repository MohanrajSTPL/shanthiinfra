package com.shanthi.network.response.enquiry;

import com.shanthi.model.enquiry.LeadStatus;
import com.shanthi.network.response.HeaderResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeadStatusResponse extends HeaderResponse {
    @SerializedName("resp")
    @Expose
    private List<LeadStatus> leadStatusList = null;

    public List<LeadStatus> getLeadStatusList() {
        return leadStatusList;
    }

    public void setLeadStatusList(List<LeadStatus> leadStatusList) {
        this.leadStatusList = leadStatusList;
    }
}