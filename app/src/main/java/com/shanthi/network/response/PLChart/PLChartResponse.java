package com.shanthi.network.response.PLChart;

import com.shanthi.model.shanthiInfra.PL;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PLChartResponse {

    @SerializedName("PL")
    @Expose
    private List<PL> pL = null;
    @SerializedName("Response_code")
    @Expose
    private String responseCode;
    @SerializedName("Message")
    @Expose
    private String message;

    public List<PL> getPL() {
        return pL;
    }

    public void setPL(List<PL> pL) {
        this.pL = pL;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
