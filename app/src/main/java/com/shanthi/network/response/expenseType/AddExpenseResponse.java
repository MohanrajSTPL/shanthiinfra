package com.shanthi.network.response.expenseType;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddExpenseResponse {

    @SerializedName("Response_code")
    @Expose
    private String responseCode;
    @SerializedName("Message")
    @Expose
    private String message;

    public String getResponseCode()    {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
