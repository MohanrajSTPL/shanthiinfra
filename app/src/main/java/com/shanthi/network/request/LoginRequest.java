package com.shanthi.network.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginRequest {
    @SerializedName("API_KEY")
    @Expose
    private String apiKey;
    @SerializedName("METHOD")
    @Expose
    private String method;
    @SerializedName("USER_NAME")
    @Expose
    private String userName;
    @SerializedName("fld_pwd")
    @Expose
    private String password;
    @SerializedName("fld_mobile")
    @Expose
    private String mobileNumber;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
