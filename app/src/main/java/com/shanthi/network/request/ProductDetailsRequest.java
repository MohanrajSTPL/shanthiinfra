package com.shanthi.network.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductDetailsRequest {
    @SerializedName("product_id")
    @Expose
    private List<String> productId;
    @SerializedName("unit")
    @Expose
    private List<String> unit;
    @SerializedName("min_quant")
    @Expose
    private List<String> minQuantity;
    @SerializedName("max_quant")
    @Expose
    private List<String> maxQuantity;

    public List<String> getProductId() {
        return productId;
    }

    public void setProductId(List<String> productId) {
        this.productId = productId;
    }

    public List<String> getUnit() {
        return unit;
    }

    public void setUnit(List<String> unit) {
        this.unit = unit;
    }

    public List<String> getMinQuantity() {
        return minQuantity;
    }

    public void setMinQuantity(List<String> minQuantity) {
        this.minQuantity = minQuantity;
    }

    public List<String> getMaxQuantity() {
        return maxQuantity;
    }

    public void setMaxQuantity(List<String> maxQuantity) {
        this.maxQuantity = maxQuantity;
    }
}
