package com.shanthi.network.api;

import com.shanthi.model.construction.Expense;
import com.shanthi.network.response.List.NewListResponse;
import com.shanthi.network.response.PLChart.PLChartResponse;
import com.shanthi.network.response.chart.ChartClickResponse;
import com.shanthi.network.response.chart.ChartListResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ChartApi {

    @POST("read.php?")
    Observable<NewListResponse> getOrdersList(@Query ("fdate") String FromDate);

    @POST("post/getChartValues.php?")
    Observable<ChartListResponse> getChartDetails(@Body Expense expense);
    @POST("post/getPLchart.php?")
    Observable<PLChartResponse> getPLList();
    @POST("get/chartPopExpense.php?")
    Observable<ChartClickResponse> getOnclickChartdetails(@Body Expense expense);
}