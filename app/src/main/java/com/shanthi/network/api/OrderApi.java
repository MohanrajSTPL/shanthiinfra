package com.shanthi.network.api;

import com.shanthi.model.construction.Construction;
import com.shanthi.model.construction.Expense;
import com.shanthi.model.payment.AddPayment;
import com.shanthi.model.shanthiInfra.Contractor;
import com.shanthi.network.response.List.NewListResponse;
import com.shanthi.network.response.contractor.ContractorListResponse;
import com.shanthi.network.response.expenseType.AddExpenseResponse;
import com.shanthi.network.response.expenseType.ApprovalResponse;
import com.shanthi.network.response.expenseType.ContractorResponse;
import com.shanthi.network.response.expenseType.ExpenseResponse;
import com.shanthi.network.response.expenseType.UserExpenseResponse;
import com.shanthi.network.response.order.CustomerResponse;
import com.shanthi.network.response.order.DeliveryDriverResponse;
import com.shanthi.network.response.order.DeliveryResponse;
import com.shanthi.network.response.order.OrderDetailsResponse;
import com.shanthi.network.response.order.OrderItemResponse;
import com.shanthi.network.response.order.OrderResponse;
import com.shanthi.network.response.order.OrderStatusResponse;
import com.shanthi.network.response.order.SummaryDetailsListResponse;
import com.shanthi.network.response.order.UomResponse;
import com.shanthi.network.response.payment.AddPaymentResponse;
import com.shanthi.network.response.payment.PaymentListResponse;
import com.shanthi.network.response.sitestatus.SiteStatusResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface OrderApi {

    @POST("getservices.php?")
    Observable<CustomerResponse> getCustomerList(@Query("API_KEY") String apiKey, @Query("METHOD") String method);

    @POST("getservices.php?")
    Observable<OrderResponse> getOrderList(@Query("API_KEY") String apiKey, @Query("METHOD") String method,
                                           @Query("user_id") String userId, @Query("order_date") String FromDate);
    @GET("get/getContractor.php?")
    Observable<ContractorResponse> getVendor();
    @GET("get/getContractor.php?")
    Observable<ContractorResponse> getSite();
    @GET("get/getContractor.php?")
    Observable<ContractorResponse> getJob();
    @GET("get/getPayment.php?")
    Observable<AddPaymentResponse> getPayment();
    @GET("get/con_Expense.php?")
    Observable<ExpenseResponse> getSiteDropdown(@Query ("type") String etype);
//    @GET("get/con_ApprovalExp.php?")
//    Observable<ApprovalResponse> getPendingApproval();
//    @POST("get/myExpense.php?")
//    Observable<UserExpenseResponse> getExpenseList(@Body Expense sessionId);
//    @POST("get/myExpense.php?")
//    Observable<UserExpenseResponse> getExpenseList(@Body Expense sessionId);
    @GET("get/con_Expense.php?")
    Observable<ExpenseResponse> getExpenses(@Query ("type") String etype);
    @POST("get/chartPopExpense.php?")
    Observable<UserExpenseResponse> getExpenseList(@Body Expense sessionId);
    @POST("get/myExpense.php?")
    Observable<UserExpenseResponse> getMyExpenseList(@Body Expense sessionId);
    @POST("get/myDeclineList.php?")
    Observable<UserExpenseResponse> getMyDeclineList(@Body Expense sessionId);
    @POST("get/mypayments.php?")
    Observable<PaymentListResponse> getMyPaymentList();
    @POST("get/myContractExp.php?")
    Observable<ContractorListResponse> getMyContractorList(@Body Expense sessionId);
    @POST("get/con_ApprovalExp.php?")
    Observable<ApprovalResponse> getPendingApproval();
    @POST("get/getPayment.php?")
    Observable<SiteStatusResponse> getSiteStatus();
    @POST("get/con_ApprovalExp.php?")
    Observable<ApprovalResponse> getPendingContractor();
//    @POST("get/con_ApprovalExp.php?")
//    Observable<ApprovalResponse> getPendingApproval(@Body Expense expense);
//    @POST("get/myContractExp.php?")
//    Observable<ApprovalResponse> getPendingApproval(@Body Expense expense);
    @GET("post/approveExpense.php?")
    Observable<ApprovalResponse> sendApproval(@Query ("expenseid") String expId,@Query ("operation") String operation);
    @GET("post/declineandDelete.php?")
    Observable<ApprovalResponse> declineExpense(@Query ("expenseid") String expId,@Query ("operation") String operation);
    @GET("post/approvePayment.php?")
    Observable<ApprovalResponse> sendContractorApproval(@Query ("paymentid") String expId,@Query ("operation") String operation);
    @POST("post/addExpense.php?")
    Observable<AddExpenseResponse> addExpense(@Body Construction newProduct);
    @POST("post/siteCompleted.php?")
    Observable<AddExpenseResponse> updateStatus(@Query ("siteid") String siteId,@Query ("status") String item);

    @POST("post/addContract.php?")
    Observable<AddExpenseResponse> addContractor(@Body Contractor newProduct);
    @POST("post/addPayment.php?")
    Observable<AddExpenseResponse> addPayment(@Body AddPayment newProduct);
    @POST("GetDelivered.php?")
    Observable<NewListResponse> getDeliveredOrders();
    @POST("GetNewPending.php?")
    Observable<NewListResponse> getOrdersPendingList(@Query("viewstate") String viewstate);
    @POST("GetNewPending.php?")
    Observable<NewListResponse> getOrdersNewList(@Query("viewstate") String viewstate);
    @POST("GetProducts.php?")
    Observable<SummaryDetailsListResponse> getOrderDetails(@Query("invoice id") String invoiceId);

    @POST("GetProducts.php?")
    Observable<NewListResponse> getDeliverySummaryDetails(@Query("invoice id") String invoiceId,@Query("Type") String transType);

    @POST("Updatestatus.php?")
    Observable<SummaryDetailsListResponse> updateDeliveryStatus(@Query("sales_id") String salesId,@Query("item_id") String itemId);


    @POST("getservices.php?")
    Observable<OrderResponse> getOrderListByStatus(@Query("API_KEY") String apiKey, @Query("METHOD") String method,
                                           @Query("user_id") String userId, @Query("status") String status);

    @POST("getservices.php?")
    Observable<OrderResponse> getListDate(@Query("API_KEY") String apiKey, @Query("METHOD") String method,
                                           @Query("user_id") String userId, @Query("order_date") String ListDate);

    @POST("getservices.php?")
    Observable<SummaryDetailsListResponse> getOrderDetails(@Query("API_KEY") String apiKey, @Query("METHOD") String method,
                                                           @Query("user_id") String userId, @Query("sale_id") String orderId,@Query("status") Integer status);

    @POST("getservices.php?")
    Observable<DeliveryResponse> getDeliveryDetails(@Query("API_KEY") String apiKey, @Query("METHOD") String method,
                                                    @Query("user_id") String userId, @Query("sale_id") String orderId,@Query("status") Integer status);

    @POST("getservices.php?")
    Observable<DeliveryResponse> getDispatchDetails(@Query("API_KEY") String apiKey, @Query("METHOD") String method,
                                                    @Query("user_id") String userId, @Query("sale_id") String orderId,@Query("status") Integer status);
    @POST("getservices.php?")
    Observable<DeliveryDriverResponse> getDeliveryDriverDetails(@Query("API_KEY") String apiKey, @Query("METHOD") String method,
                                                                @Query("user_id") String userId, @Query("sale_id") String orderId, @Query("status") Integer status);

    @POST("getservices.php?")
    Observable<OrderItemResponse> getDelivery(@Query("API_KEY") String apiKey, @Query("METHOD") String method,
                                              @Query("user_id") String userId);

    @POST("getservices.php?")
    Observable<UomResponse> getUom(@Query("API_KEY") String apiKey, @Query("METHOD") String method,
                                   @Query("item_id") String item_id,
                                   @Query("order_id") String order_id);

    @POST("getservices.php?")
    Observable<OrderStatusResponse> getOrderStatus(@Query("API_KEY") String apiKey, @Query("METHOD") String method,
                                                   @Query("user_id") String userId);

    @POST("getservices.php?")
    Observable<OrderDetailsResponse> addOrderItem(@Query("API_KEY") String apiKey, @Query("METHOD") String method, @Query("user_id") String userId,
                                                  @Query("order_id") String orderId,@Query("truck_id") String truckId,
                                                  @Query("tracking") String truckNumber,
                                                  @Query("delivery_person") String driverName,
                                                  @Query("del_per_mobile") String delMobile,
                                                  @Query("dispatch_date") String dispatchDate,
                                                  @Query("dispatch_time") String dispatchTime,
                                                  @Query("item_id") String itemid,
                                                  @Query("item_qty") String itemqty,
                                                  @Query("quantity_value") String quantityValue);

    @POST("getservices.php?")
    Observable<OrderDetailsResponse> addDelivery(@Query("API_KEY") String apiKey, @Query("METHOD") String method, @Query("user_id") String userId,
                                             @Query("order_id") String orderId, @Query("delivery_date") String deliveryDate,
                                             @Query("delivery_time") String deliveryTime);
}