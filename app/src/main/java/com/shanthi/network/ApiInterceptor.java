package com.shanthi.network;

import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ApiInterceptor implements Interceptor {
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request();

        // LoginResponse loginResponse = AccountUtils.getLogin(ACCOUNT_PREFS);

        /*if (loginResponse == null) {
            return chain.proceed(request);
        }*/

        //String token = loginResponse.getAuthorization();

        // Adding token as Authorization header for every request
        /*request = request.newBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", token).build();*/
        return chain.proceed(request);
    }
}
