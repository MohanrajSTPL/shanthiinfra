package com.shanthi.service.fcm;

import com.shanthi.network.ApiClient;
import com.shanthi.network.api.FcmApi;
import com.shanthi.network.response.HeaderResponse;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

import static com.shanthi.constant.Constants.API_KEY;


import static com.shanthi.constant.Constants.METHOD_DELIVERY_TOKEN_UPDATE;


public class FcmService {

    public static Observable<HeaderResponse> addDevice(String fcmToken, String userId) {
        return ApiClient.getInstance().createService(FcmApi.class)
                .addDevice(API_KEY, METHOD_DELIVERY_TOKEN_UPDATE, userId, fcmToken)
                .observeOn(AndroidSchedulers.mainThread());
    }
}
