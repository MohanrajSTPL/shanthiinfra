package com.shanthi.service.onBoarding;

import com.shanthi.model.onBoarding.Login;
import com.shanthi.network.ApiClient;
import com.shanthi.network.api.OnBoardingApi;
import com.shanthi.network.response.HeaderResponse;
import com.shanthi.network.response.onBoarding.LoginResponse;
import com.shanthi.utils.AccountUtils;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

import static com.shanthi.constant.Constants.ACCOUNT_PREFS;
import static com.shanthi.constant.Constants.API_KEY;

import static com.shanthi.constant.Constants.METHOD_CHANGE_PASSWORD;

public class OnBoardingService {
    public static Observable<LoginResponse> logIn(Login login) {

        return ApiClient.getInstance().createService(OnBoardingApi.class)
                .logIn(login)
                .map(loginResponse -> {
                    saveLogin(loginResponse);
                    return loginResponse;
                })
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<HeaderResponse> changePassword(String userId, String oldPassword, String newPassword) {
        return ApiClient.getInstance().createService(OnBoardingApi.class)
                .changePassword(API_KEY,METHOD_CHANGE_PASSWORD,userId, oldPassword, newPassword)
                .observeOn(AndroidSchedulers.mainThread());
    }
    // Save login
    private static void saveLogin(LoginResponse response) {
      //  if (response.getResponseCode() != 0) {
            //Save login response
            AccountUtils.saveLogin(ACCOUNT_PREFS, response);

    }
}
