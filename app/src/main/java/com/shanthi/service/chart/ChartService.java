package com.shanthi.service.chart;

import com.shanthi.model.construction.Expense;
import com.shanthi.network.ApiClient;
import com.shanthi.network.api.ChartApi;
import com.shanthi.network.response.PLChart.PLChartResponse;
import com.shanthi.network.response.chart.ChartClickResponse;
import com.shanthi.network.response.chart.ChartListResponse;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class ChartService {
    public static Observable<ChartListResponse> getChartDetails(Expense expense) {

        return ApiClient.getInstance().createService(ChartApi.class)
                .getChartDetails(expense)
                .observeOn(AndroidSchedulers.mainThread());
    }
  public static Observable<PLChartResponse> getPLList() {

        return ApiClient.getInstance().createService(ChartApi.class)
                .getPLList()
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<ChartClickResponse> getOnclickChartdetails(Expense expense) {
        expense.getSessionid();
        expense.getUsertype();
        expense.getExpenseid();
        return ApiClient.getInstance().createService(ChartApi.class)
                .getOnclickChartdetails(expense)
                .observeOn(AndroidSchedulers.mainThread());
    }
}
