package com.shanthi.utils;

import android.net.Uri;

import java.io.File;
import java.net.UnknownHostException;
import java.text.Format;
import java.text.NumberFormat;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.HttpException;

public class AppUtils {
    public static boolean isEmpty(String text) {
        return text == null || text.equals("") || text.matches(" *") || text.equals("null");
    }

    public static boolean isValidEmail(String email) {
        return email != null && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidPassword(String password) {
        return (password != null && password.length() >= 8);
    }

    public static boolean isValidMobileNo(String mobileNo) {
        return (mobileNo != null && mobileNo.length() == 10);
    }

    public static boolean isValidNo(String mobileNo, int length) {
        return (mobileNo != null && mobileNo.length() == length);
    }

    public static String getTitle(int pos) {
        return getTitles()[pos];
    }

    public static String[] getTitles() {
        return new String[]{"Manage Job Card", "Types Of Service",
                "Fuel And Tyre", "Vehicle Damage", "Current Vehicle Accessories List", "Work to be Done",
                "Additional Accessories", "Invoice", "Customer Signature", "Check List"};
      /*  return new String[]{"Manage Job Card", "Service Details", "Types Of Service",
                "Fuel And Tyre", "Vehicle Damage", "Current Vehicle Accessories List",
                "Work to be Done", "Additional Accessories", "Invoice", "Customer Signature"};*/
    }

    public static String getPageNo(int pos, int total) {
        String[] titles = new String[total];
        for (int i = 0; i < total; i++) {
            titles[i] = "(" + (i + 1) + "/" + total + ")";
        }
        return titles[pos];
    }

    // Check run time exceptions
    public static String checkThrowable(Throwable throwable) {
        // Check for internet connection
        if (throwable instanceof UnknownHostException) {
            return "Please check your internet connection";
        }
        if (throwable instanceof HttpException) {
            // Check for server http error
            HttpException e = (HttpException) throwable;
            int responseCode = e.code();
            switch (responseCode) {
                case 404:
                    return "URL not found";
                case 500:
                    return "Server broken";
                /*default:
                    ApiFailure error = ErrorUtils.parseErrorBody(e.response());
                    return error.getErrorMessage();*/
            }
        }
        return "Something went wrong";
    }

    private static final String MULTIPART_FORM_DATA = "multipart/form-data";

    public static MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {
        // use the FileUtils to get the actual file by uri
        File file = new File(fileUri.getPath());
        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), file);

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    public static RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                MediaType.parse(MULTIPART_FORM_DATA), descriptionString);
    }

    public static String booleanToString(boolean value) {
        return value ? "1" : "0";
    }

    public static String[] splitStringBySeparator(String string, String separatorString) {
        return string.split(separatorString);
    }

    public static String getMonthHrMinStr(int month) {
        return month > 9 ? "" + month : "0" + month;
    }

    public static boolean isStartWith(String text, String startString) {
        return !text.startsWith(startString);
    }

    public static String rupeesFormat(String rupees) {
        if (rupees != null && !rupees.equalsIgnoreCase("null") && !rupees.isEmpty()) {
            Format format = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
            return format.format(Double.valueOf(rupees));
        } else {
            return "";
        }
    }

    public static String numberFormat(String number) {
        if (number != null && !number.equalsIgnoreCase("null") && !number.isEmpty()) {
            return NumberFormat.getNumberInstance(new Locale("en", "in")).format(Double.valueOf(number));
        } else {
            return "";
        }
    }


    public static Double calculatePercentageAmount(Double totalAmount, Double percentage) {
        return (totalAmount * (percentage / 100.0f));
    }
}