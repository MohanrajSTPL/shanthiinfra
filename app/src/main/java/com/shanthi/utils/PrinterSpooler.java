package com.shanthi.utils;


import android.os.AsyncTask;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class PrinterSpooler extends AsyncTask<String, String, String> {

    @Override
    protected String doInBackground(String... strings) {
        try {
            System.out.println("response     shared_ip_addressssss    :" + "192.168.100.1");
            Socket sock = new Socket("192.168.100.1", 9100);
            PrintWriter oStream = new PrintWriter(sock.getOutputStream());
            oStream.println("        SMARTIFICIA TECHNOLOGIES                         ");
            oStream.close();
            sock.close();

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
