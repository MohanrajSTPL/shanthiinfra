package com.shanthi.utils;

import android.content.Context;
import android.text.TextUtils;
import android.text.format.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class TimeUtils {
    static final SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    private static final int SECOND = 1000;
    private static final int MINUTE = 60 * SECOND;
    private static final int HOUR = 60 * MINUTE;
    public static final int DAY = 24 * HOUR;

    private static final SimpleDateFormat[] ACCEPTED_TIMESTAMP_FORMATS = {
            new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.getDefault()),
            new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy", Locale.getDefault()),
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()),
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z", Locale.getDefault()),
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault()),
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault()),
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault()),
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss Z", Locale.getDefault()),
            new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    };
    private static final SimpleDateFormat VALID_IFMODIFIEDSINCE_FORMAT =
            new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.getDefault());

    public static Date parseTimestamp(String timestamp) {
        //it may be an integer - milliseconds
        try {
            if (timestamp.matches("[0-9]+")) {
                long longDate = Long.parseLong(timestamp);
                return new Date(longDate);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        for (SimpleDateFormat format : ACCEPTED_TIMESTAMP_FORMATS) {
            format.setTimeZone(TimeZone.getDefault());
            try {
                return format.parse(timestamp);
            } catch (ParseException ex) {
                continue;
            }
        }
        // didnt match any format
        return null;
    }

    public static boolean isSameDay(long time1, long time2, Context context) {
        TimeZone displayTimeZone = TimeUtils.getTimeZone();
        Calendar cal1 = Calendar.getInstance(displayTimeZone);
        Calendar cal2 = Calendar.getInstance(displayTimeZone);
        cal1.setTimeInMillis(time1);
        cal2.setTimeInMillis(time2);
        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
    }

    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null)
            throw new IllegalArgumentException("Calender object cannot be null");
        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
    }

    public static boolean isValidFormatForIfModifiedSinceHeader(String timestamp) {
        try {
            return VALID_IFMODIFIEDSINCE_FORMAT.parse(timestamp) != null;
        } catch (Exception ex) {
            return false;
        }
    }

    public static long timestampToMillis(String timestamp, long defaultValue) {
        if (TextUtils.isEmpty(timestamp)) {
            return defaultValue;
        }
        Date d = parseTimestamp(timestamp);
        return d == null ? defaultValue : d.getTime();
    }

    /**
     * Format a {@code date} honoring the app preference for using device timezone.
     * {@code Context} is used to lookup the shared preference settings.
     */
    public static String formatShortDate(Context context, Date date) {
        StringBuilder sb = new StringBuilder();
        Formatter formatter = new Formatter(sb);
        return DateUtils.formatDateRange(context, formatter, date.getTime(), date.getTime(),
                DateUtils.FORMAT_ABBREV_ALL | DateUtils.FORMAT_NO_YEAR,
                getDisplayTimeZone(context).getID()).toString();
    }

    public static String formatShortTime(Context context, Date time) {
        // Android DateFormatter will honor the user's current settings.
        DateFormat format = android.text.format.DateFormat.getTimeFormat(context);
        // Override with Timezone based on settings since users can override their phone's timezone
        // with Pacific time zones.
        TimeZone tz = getDisplayTimeZone(context);
        if (tz != null) {
            format.setTimeZone(tz);
        }
        return format.format(time);
    }

    public static TimeZone getTimeZone() {
        return TimeZone.getDefault();
    }

    public static long getCurrentTimeInMs() {
        return System.currentTimeMillis();
    }

    /**
     * Returns "Today", "Tomorrow", "Yesterday", or a short date format.
     */
    public static String formatHumanFriendlyShortDate(final Context context, long timestamp) {
        long localTimestamp, localTime;
        long now = TimeUtils.getCurrentTimeInMs();

        TimeZone tz = getDisplayTimeZone(context);
        localTimestamp = timestamp + tz.getOffset(timestamp);
        localTime = now + tz.getOffset(now);

        long dayOrd = localTimestamp / 86400000L;
        long nowOrd = localTime / 86400000L;

        if (dayOrd == nowOrd) {
            //return context.getString(R.string.day_title_today);
            return new SimpleDateFormat("hh:mm a", Locale.getDefault())
                    .format(new Date(timestamp))
                    .replace("am", "AM")
                    .replace("pm", "PM");
        } else if (dayOrd == nowOrd - 1) {
            return "Yesterday";
        } else if (dayOrd == nowOrd + 1) {
            return "Tomorrow";
        } else {
            return formatShortDate(context, new Date(timestamp));
        }
    }

    public static TimeZone getDisplayTimeZone(Context context) {
        return TimeZone.getDefault();
    }

    public static long diff(Date date1, Date date2) {
        return date1.getTime() - date2.getTime();
    }

    public static long diffInSeconds(Date date1, Date date2) {
        return TimeUnit.MILLISECONDS.toSeconds(date1.getTime() - date2.getTime());
    }

    public static long diffInMinutes(Date date1, Date date2) {
        return TimeUnit.MILLISECONDS.toMinutes(date1.getTime() - date2.getTime());
    }

    public static long diffInHours(Date date1, Date date2) {
        return TimeUnit.MILLISECONDS.toHours(date1.getTime() - date2.getTime());
    }

    public static long diffInDays(Date date1, Date date2) {
        return TimeUnit.MILLISECONDS.toDays(date1.getTime() - date2.getTime());
    }

    public static Date getCurrentTime() {
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss", Locale.getDefault());

        //Local time zone
        SimpleDateFormat dateFormatLocal = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss", Locale.getDefault());

        //Time in GMT
        try {
            return dateFormatLocal.parse(dateFormatGmt.format(new Date()));
        } catch (ParseException e) {
            return null;
        }
    }

    public static boolean isToday(Date date) {
        if (date == null)
            throw new IllegalArgumentException("Date object cannot be null");
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date);

        Calendar cal2 = Calendar.getInstance();

        return isSameDay(cal1, cal2);
    }

    public static boolean isYesterday(Date date) {
        if (date == null)
            throw new IllegalArgumentException("Date object cannot be null");
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date);

        Calendar cal2 = Calendar.getInstance();
        if ((cal2.get(Calendar.DATE) - cal1.get(Calendar.DATE)) == 1) {
            return true;
        } else {
            return false;
        }
    }

    // Convert from yyyy-MM-dd to dd MMM,yyyy
    public static String convertDateString(String string) {
        DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        DateFormat toFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        Date date = null;
        try {
            date = fromFormat.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return toFormat.format(date);
    }

    public static String convertDateStringWithTime(String string) {
        DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        DateFormat toFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.getDefault());
        Date date = null;
        try {
            date = fromFormat.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return toFormat.format(date);
    }

    public static String convertDateStringDate(String string) {
        DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        DateFormat toFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        Date date = null;
        try {
            date = fromFormat.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return toFormat.format(date);
    }


    public static long convertDateFormatStringDate(String string) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = sdf.parse(string);
        return date.getTime();
    }


    public static String getCurrentTimeZone() {
        return Calendar.getInstance().getTimeZone().getID();
    }

    public static String getDateString(String dateTimeString) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        DateFormat outputFormat = new SimpleDateFormat("dd MMM,yyyy", Locale.getDefault());

        String convertedDate = null;
        try {
            convertedDate = outputFormat.format(inputFormat.parse(dateTimeString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedDate;
    }

    public static String getDateString1(String dateTimeString) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        String convertedDate = null;
        try {
            convertedDate = outputFormat.format(inputFormat.parse(dateTimeString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedDate;
    }

    public static String getTimeString(String dateTimeString) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        DateFormat outputFormat = new SimpleDateFormat("KK:mm a", Locale.getDefault());

        String convertedTime = null;
        try {
            convertedTime = outputFormat.format(inputFormat.parse(dateTimeString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedTime != null ? convertedTime
                .replace("am", "A.M.")
                .replace("p.m.", "P.M.") : null;
    }

    public static String getTimeString1(String dateTimeString) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
        DateFormat outputFormat = new SimpleDateFormat("KK:mm a", Locale.getDefault());

        String convertedTime = null;
        try {
            convertedTime = outputFormat.format(inputFormat.parse(dateTimeString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedTime != null ? convertedTime
                .replace("a.m.", "AM")
                .replace("p.m.", "PM") : null;
    }

    public static String getTimeStringInGMTZone(String dateTimeString) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        inputFormat.setTimeZone(TimeZone.getTimeZone(("GMT")));
        DateFormat outputFormat = new SimpleDateFormat("KK:mm a", Locale.getDefault());

        String convertedTime = null;
        try {
            convertedTime = outputFormat.format(inputFormat.parse(dateTimeString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedTime != null ? convertedTime
                .replace("am", "AM")
                .replace("pm", "PM") : null;
    }

    public static String getDateTimeString(String dateTimeString) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy KK:mm a", Locale.getDefault());

        String convertedTime = null;
        try {
            convertedTime = outputFormat.format(inputFormat.parse(dateTimeString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedTime != null ? convertedTime
                .replace("am", "AM")
                .replace("pm", "PM") : null;
    }

    public static String getDateTimeString1(String dateTimeString) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy KK:mm a", Locale.getDefault());

        String convertedTime = null;
        try {
            convertedTime = outputFormat.format(inputFormat.parse(dateTimeString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedTime != null ? convertedTime
                .replace("am", "AM")
                .replace("pm", "PM") : null;
    }

    public static String getCurrentDateString(Date date) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return format.format(date);
    }

    public static String getCurrentDateString() {
        Calendar c = Calendar.getInstance();
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        return format.format(c.getTime());
    }

    public static String getStringFromDate(Date date) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return format.format(date);
    }

    public static String getStringFromDateString(String date) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return format.format(date);
    }

    public static Date getDateFromString(String date) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date d = null;
        try {
            d = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    public static Date getDateFromStringWithTimeZone(String date) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        TimeZone utcZone = TimeZone.getTimeZone("UTC");
        format.setTimeZone(utcZone);
        Date d = null;

        try {
            d = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    public static Date getDateFromDateStringOnly(String date) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        Date d = null;

        try {
            d = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    // Get time with local time zone
    public static String getLocalTimeZoneTime(String dateString) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        TimeZone utcZone = TimeZone.getTimeZone("UTC");
        simpleDateFormat.setTimeZone(utcZone);
        Date myDate = null;
        try {
            myDate = simpleDateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(myDate);
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy KK:mm a", Locale.getDefault());
        outputFormat.setTimeZone(TimeZone.getDefault());
        String convertedTime = outputFormat.format(myDate);
        return convertedTime != null ? convertedTime
                .replace("am", "AM")
                .replace("pm", "PM") : null;
    }

    // Get time with local time zone
    public static String getDateWithTimeZone(String dateString) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date myDate = null;
        try {
            myDate = simpleDateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(myDate);
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy KK:mm a", Locale.getDefault());
        outputFormat.setTimeZone(TimeZone.getDefault());
        String convertedTime = outputFormat.format(myDate);
        return convertedTime != null ? convertedTime
                .replace("am", "AM")
                .replace("pm", "PM") : null;
    }

    // Calculate dates between two dates
    public static List<Date> getDates(String dateString1, String dateString2) {
        ArrayList<Date> dates = new ArrayList<Date>();
        SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1.parse(dateString1);
            date2 = df1.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while (!cal1.after(cal2)) {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }

    public static String getStringFromDate2(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMMM", Locale.getDefault());
        return sdf.format(date);
    }

    public static String getDayHumanReadable(final Context context, Date date) {
        String day;
        if (TimeUtils.isToday(date)) {
            day = "Today";
        } else if (TimeUtils.isYesterday(date)) {
            day = "Yesterday";
        } else {
            day = getStringFromDate2(date);
        }
        return day;
    }

    // Get date string with AM/PM from date
    public static String getDateStrWithAMPM(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.ENGLISH);
        return sdf.format(date).replace("am", "AM")
                .replace("pm", "PM");
    }

    public static String get24HourFormat(String dateTime12HourFormat) {
        //Format of the date defined in the input String
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm aa", Locale.ENGLISH);
        //Desired format: 24 hour format: Change the pattern as per the need
        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date date = null;
        String outputDateTime24HourFormat = null;
        try {
            //Converting the input String to Date
            date = df.parse(dateTime12HourFormat);
            //Changing the format of date and storing it in String
            outputDateTime24HourFormat = outputFormat.format(date);
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return outputDateTime24HourFormat;
    }

    public static String getTimeStringFromDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa", Locale.getDefault());
        String convertedTime = sdf.format(date);
        return convertedTime != null ? convertedTime
                .replace("a.m.", "am")
                .replace("p.m.", "pm") : null;
    }

    // Date format like 21 oct 2018
    public static String getDateFormatFromDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        return sdf.format(date);
    }

    // Date format like 21 oct 2018
    public static String getDateFormatFromString(String dateString) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = null;
        try {
            date = simpleDateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf.format(date);
    }

    // Date format like 12 pm
    public static String getTimeFormatFromString(String dateString) {

        SimpleDateFormat sdf = new SimpleDateFormat("hh aa", Locale.getDefault());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
        Date date = null;
        try {
            date = simpleDateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String convertedTime = sdf.format(date);
        return convertedTime != null ? convertedTime
                .replace("a.m.", "am")
                .replace("p.m.", "pm") : null;
    }

    public static String getDayHumanReadable(Date date) {
        String day;
        if (TimeUtils.isToday(date)) {
            day = "Today";
        } else if (TimeUtils.isYesterday(date)) {
            day = "Yesterday";
        } else {
            day = getStringFromDate2(date);
        }
        return day;
    }
}
