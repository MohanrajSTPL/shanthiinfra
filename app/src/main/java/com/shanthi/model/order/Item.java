package com.shanthi.model.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class  Item {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("purchase_cost")
    @Expose
    private String purchaseCost;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("tax")
    @Expose
    private String tax;
    @SerializedName("subtotal")
    @Expose
    private String subtotal;
    @SerializedName("item_delivered")
    @Expose
    private String item_delivered;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("pending_qty")
    @Expose
    private String pendingQty;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPurchaseCost() {
        return purchaseCost;
    }

    public void setPurchaseCost(String purchaseCost) {
        this.purchaseCost = purchaseCost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getItem_delivered() {
        return item_delivered;
    }

    public void setItem_delivered(String item_delivered) {
        this.item_delivered = item_delivered;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getpendingQty() {
        return pendingQty;
    }

    public void setpendingQty(String pendingQty) {
        this.pendingQty = pendingQty;
    }



}
