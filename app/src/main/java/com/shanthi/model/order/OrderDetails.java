package com.shanthi.model.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDetails {

    @SerializedName("id")
    @Expose
    private String orderId;
    @SerializedName("omgmt_quot_id")
    @Expose
    private String quotationId;
    @SerializedName("delivery_person")
    @Expose
    private String driverName;
    @SerializedName("dispatch_date")
    @Expose
    private String dispatchDate;
    @SerializedName("delivery_person")
    @Expose
    private String dispatchTime;
    @SerializedName("omgmt_cust_id")
    @Expose
    private String orderCustomerId;
    @SerializedName("omgmt_custtype_id")
    @Expose
    private String orderCustomerTypeId;
    @SerializedName("delivery_date")
    @Expose
    private String deliveryDate;

    @SerializedName("delivery_time")
    @Expose
    private String deliveryTime;

    @SerializedName("invoice_date")
    @Expose
    private String orderDate;
    @SerializedName("omgmt_invc_no")
    @Expose
    private String invoiceNumber;
    @SerializedName("omgmt_sub_tot")
    @Expose
    private String orderSubTotal;
    @SerializedName("omgmt_gst_tot")
    @Expose
    private String orderGstTotal;
    @SerializedName("omgmt_cgst_tot")
    @Expose
    private String orderCgstTotal;
    @SerializedName("omgmt_sgst_tot")
    @Expose
    private String orderSgstTotal;
    @SerializedName("grand_total")
    @Expose
    private String orderGrandTotal;
    @SerializedName("omgmt_round_off")
    @Expose
    private String orderRoundOff;
    @SerializedName("omgmt_net_amt")
    @Expose
    private String orderNetAmount;
    @SerializedName("omgmt_dlvry_chrg")
    @Expose
    private String orderDeliveryChrg;
    @SerializedName("omgmt_trans_amt")
    @Expose
    private String orderTransportationAmount;
    @SerializedName("omgmt_terms")
    @Expose
    private String orderTerms;
    @SerializedName("omgmt_drvr_name")
    @Expose
    private String orderDeliveryName;
    @SerializedName("omgmt_drvr_mob_no")
    @Expose
    private String orderDeliveryMobileNumber;
    @SerializedName("omgmt_truck_no")
    @Expose
    private String orderTruckNo;
    @SerializedName("omgmt_purch_no")
    @Expose
    private String orderPurchaseNumber;
    @SerializedName("omgmt_purch_date")
    @Expose
    private String orderPurchaseDate;
    @SerializedName("omgmt_status")
    @Expose
    private String orderStatus;
    @SerializedName("omgmt_bank_id")
    @Expose
    private String orderBankId;
    @SerializedName("omgmt_declrtn")
    @Expose
    private String omgmtDeclrtn;
    @SerializedName("omgmt_disp_thru")
    @Expose
    private String omgmtDispThru;
    @SerializedName("omgmt_dlvry_addr")
    @Expose
    private String omgmtDlvryAddr;
    @SerializedName("omgmt_same_addr")
    @Expose
    private String omgmtSameAddr;
    @SerializedName("truck_id")
    @Expose
    private String truck_id;
    @SerializedName("om_ln_itm_item_id")
    @Expose
    private String omLnItmItemId;
    @SerializedName("del_per_mobile")
    @Expose
    private String delMobile;
    @SerializedName("tracking")
    @Expose
    private String omLnItmPrice;
    @SerializedName("delivery_status")
    @Expose
    private String deliveryStatus;
    @SerializedName("om_ln_itm_rate")
    @Expose
    private String omLnItmRate;
    @SerializedName("om_ln_itm_gst_per")
    @Expose
    private String omLnItmGstPer;
    @SerializedName("om_ln_itm_gst_amt")
    @Expose
    private String omLnItmGstAmt;
    @SerializedName("om_ln_itm_cgst_per")
    @Expose
    private String omLnItmCgstPer;
    @SerializedName("om_ln_itm_cgst_amt")
    @Expose
    private String omLnItmCgstAmt;
    @SerializedName("om_ln_itm_sgst_per")
    @Expose
    private String omLnItmSgstPer;
    @SerializedName("om_ln_itm_sgst_amt")
    @Expose
    private String omLnItmSgstAmt;
    @SerializedName("om_ln_itm_tot")
    @Expose
    private String omLnItmTot;
//    @SerializedName("driver_name")
//    @Expose
//    private String prodName;
    @SerializedName("om_ln_itm_qty")
    @Expose
    private String om_ln_itm_qty;
    @SerializedName("om_ln_itm_tot_in_word")
    @Expose
    private String omInItmTotalInWord;
    @SerializedName("quantity_value")
    @Expose
    private String quantityValue;

    public String getOm_ln_itm_qty() {
        return om_ln_itm_qty;
    }

    public void setOm_ln_itm_qty(String om_ln_itm_qty) {
        this.om_ln_itm_qty = om_ln_itm_qty;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }


    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }
    public String getQuotationId() {
        return quotationId;
    }

    public void setQuotationId(String quotationId) {
        this.quotationId = quotationId;
    }

    public String getOrderCustomerId() {
        return orderCustomerId;
    }

    public void setOrderCustomerId(String orderCustomerId) {
        this.orderCustomerId = orderCustomerId;
    }
    public String getDispatchDate() {
        return dispatchDate;
    }

    public void setDispatchDate(String dispatchDate) {
        this.dispatchDate = dispatchDate;
    }
    public String getDispatchTime() {
        return dispatchTime;
    }

    public void setDispatchTime(String dispatchTime) {
        this.dispatchTime = dispatchTime;
    }
    public String getOrderCustomerTypeId() {
        return orderCustomerTypeId;
    }

    public void setOrderCustomerTypeId(String orderCustomerTypeId) {
        this.orderCustomerTypeId = orderCustomerTypeId;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getOrderSubTotal() {
        return orderSubTotal;
    }

    public void setOrderSubTotal(String orderSubTotal) {
        this.orderSubTotal = orderSubTotal;
    }

    public String getOrderGstTotal() {
        return orderGstTotal;
    }

    public void setOrderGstTotal(String orderGstTotal) {
        this.orderGstTotal = orderGstTotal;
    }

    public String getOrderCgstTotal() {
        return orderCgstTotal;
    }

    public void setOrderCgstTotal(String orderCgstTotal) {
        this.orderCgstTotal = orderCgstTotal;
    }

    public String getOrderSgstTotal() {
        return orderSgstTotal;
    }

    public void setOrderSgstTotal(String orderSgstTotal) {
        this.orderSgstTotal = orderSgstTotal;
    }

    public String getOrderGrandTotal() {
        return orderGrandTotal;
    }

    public void setOrderGrandTotal(String orderGrandTotal) {
        this.orderGrandTotal = orderGrandTotal;
    }

    public String getOrderRoundOff() {
        return orderRoundOff;
    }

    public void setOrderRoundOff(String orderRoundOff) {
        this.orderRoundOff = orderRoundOff;
    }

    public String getOrderNetAmount() {
        return orderNetAmount;
    }

    public void setOrderNetAmount(String orderNetAmount) {
        this.orderNetAmount = orderNetAmount;
    }

    public String getOrderDeliveryChrg() {
        return orderDeliveryChrg;
    }

    public void setOrderDeliveryChrg(String orderDeliveryChrg) {
        this.orderDeliveryChrg = orderDeliveryChrg;
    }

    public String getOrderTransportationAmount() {
        return orderTransportationAmount;
    }

    public void setOrderTransportationAmount(String orderTransportationAmount) {
        this.orderTransportationAmount = orderTransportationAmount;
    }

    public String getOrderTerms() {
        return orderTerms;
    }

    public void setOrderTerms(String orderTerms) {
        this.orderTerms = orderTerms;
    }

    public String getOrderDeliveryName() {
        return orderDeliveryName;
    }

    public void setOrderDeliveryName(String orderDeliveryName) {
        this.orderDeliveryName = orderDeliveryName;
    }

    public String getOrderDeliveryMobileNumber() {
        return orderDeliveryMobileNumber;
    }

    public void setOrderDeliveryMobileNumber(String orderDeliveryMobileNumber) {
        this.orderDeliveryMobileNumber = orderDeliveryMobileNumber;
    }

    public String getOrderTruckNo() {
        return orderTruckNo;
    }

    public void setOrderTruckNo(String orderTruckNo) {
        this.orderTruckNo = orderTruckNo;
    }

    public String getOrderPurchaseNumber() {
        return orderPurchaseNumber;
    }

    public void setOrderPurchaseNumber(String orderPurchaseNumber) {
        this.orderPurchaseNumber = orderPurchaseNumber;
    }

    public String getOrderPurchaseDate() {
        return orderPurchaseDate;
    }

    public void setOrderPurchaseDate(String orderPurchaseDate) {
        this.orderPurchaseDate = orderPurchaseDate;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderBankId() {
        return orderBankId;
    }

    public void setOrderBankId(String orderBankId) {
        this.orderBankId = orderBankId;
    }

    public String getOmgmtDeclrtn() {
        return omgmtDeclrtn;
    }

    public void setOmgmtDeclrtn(String omgmtDeclrtn) {
        this.omgmtDeclrtn = omgmtDeclrtn;
    }

    public String getOmgmtDispThru() {
        return omgmtDispThru;
    }

    public void setOmgmtDispThru(String omgmtDispThru) {
        this.omgmtDispThru = omgmtDispThru;
    }

    public String getOmgmtDlvryAddr() {
        return omgmtDlvryAddr;
    }

    public void setOmgmtDlvryAddr(String omgmtDlvryAddr) {
        this.omgmtDlvryAddr = omgmtDlvryAddr;
    }

    public String getOmgmtSameAddr() {
        return omgmtSameAddr;
    }

    public void setOmgmtSameAddr(String omgmtSameAddr) {
        this.omgmtSameAddr = omgmtSameAddr;
    }

    public String getTrucId() {
        return truck_id;
    }

    public void setTruckId(String truck_id) {
        this.truck_id = truck_id;
    }

    public String getOmLnItmItemId() {
        return omLnItmItemId;
    }

    public void setOmLnItmItemId(String omLnItmItemId) {
        this.omLnItmItemId = omLnItmItemId;
    }

    public String getDelMobile() {
        return delMobile;
    }

    public void setDelMobile(String delMobile) {
        this.delMobile = delMobile;
    }

    public String getTruckNumber() {
        return omLnItmPrice;
    }

    public void setTruckNumber(String omLnItmPrice) {
        this.omLnItmPrice = omLnItmPrice;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getOmLnItmRate() {
        return omLnItmRate;
    }

    public void setOmLnItmRate(String omLnItmRate) {
        this.omLnItmRate = omLnItmRate;
    }

    public String getOmLnItmGstPer() {
        return omLnItmGstPer;
    }

    public void setOmLnItmGstPer(String omLnItmGstPer) {
        this.omLnItmGstPer = omLnItmGstPer;
    }

    public String getOmLnItmGstAmt() {
        return omLnItmGstAmt;
    }

    public void setOmLnItmGstAmt(String omLnItmGstAmt) {
        this.omLnItmGstAmt = omLnItmGstAmt;
    }

    public String getOmLnItmCgstPer() {
        return omLnItmCgstPer;
    }

    public void setOmLnItmCgstPer(String omLnItmCgstPer) {
        this.omLnItmCgstPer = omLnItmCgstPer;
    }

    public String getOmLnItmCgstAmt() {
        return omLnItmCgstAmt;
    }

    public void setOmLnItmCgstAmt(String omLnItmCgstAmt) {
        this.omLnItmCgstAmt = omLnItmCgstAmt;
    }

    public String getOmLnItmSgstPer() {
        return omLnItmSgstPer;
    }

    public void setOmLnItmSgstPer(String omLnItmSgstPer) {
        this.omLnItmSgstPer = omLnItmSgstPer;
    }

    public String getOmLnItmSgstAmt() {
        return omLnItmSgstAmt;
    }

    public void setOmLnItmSgstAmt(String omLnItmSgstAmt) {
        this.omLnItmSgstAmt = omLnItmSgstAmt;
    }

    public String getOmLnItmTot() {
        return omLnItmTot;
    }

    public void setOmLnItmTot(String omLnItmTot) {
        this.omLnItmTot = omLnItmTot;
    }
//
//    public String getProdName() {
//        return prodName;
//    }
//
//    public void setProdName(String prodName) {
//        this.prodName = prodName;
//    }

    public String getOmInItmTotalInWord() {
        return omInItmTotalInWord;
    }

    public void setOmInItmTotalInWord(String omInItmTotalInWord) {
        this.omInItmTotalInWord = omInItmTotalInWord;
    }

    public String getQuantityValue() {
        return quantityValue;
    }

    public void setQuantityValue(String quantityValue) {
        this.quantityValue = quantityValue;
    }
}