package com.shanthi.model.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Uom {
    @SerializedName("id")
    @Expose
    private String uomId;
    @SerializedName("uom_name")
    @Expose
    private String uomName;
    @SerializedName("uom_value")
    @Expose
    private String uomValue;

    public String getUomId() {
        return uomId;
    }

    public void setUomId(String uomId) {
        this.uomId = uomId;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public String getUomValue() {
        return uomValue;
    }

    public void setUomValue(String uomValue) {
        this.uomValue = uomValue;
    }
}
