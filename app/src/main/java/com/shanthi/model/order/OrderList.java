package com.shanthi.model.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderList {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("customer_id")
    @Expose
    private String customerId;

    @SerializedName("customer_name")
    @Expose
    private String customerName;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("b_address")
    @Expose
    private String bAddress;
    @SerializedName("s_address")
    @Expose
    private String sAddress;
    @SerializedName("invoice_date")
    @Expose
    private String invoiceDate;
    @SerializedName("due_date")
    @Expose
    private String dueDate;
    @SerializedName("cart")
    @Expose
    private String cart;
    @SerializedName("cart_total")
    @Expose
    private String cartTotal;
    @SerializedName("grand_total")
    @Expose
    private String grandTotal;
    @SerializedName("tax")
    @Expose
    private String tax;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("amount_received")
    @Expose
    private String amountReceived;
    @SerializedName("due_payment")
    @Expose
    private String duePayment;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("p_reference")
    @Expose
    private String pReference;
    @SerializedName("order_note")
    @Expose
    private String orderNote;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("history")
    @Expose
    private String history;
    @SerializedName("sales_person")
    @Expose
    private String salesPerson;
    @SerializedName("cancel_note")
    @Expose
    private Object cancelNote;
    @SerializedName("delivery_status")
    @Expose
    private String deliveryStatus;
    @SerializedName("tracking")
    @Expose
    private Object tracking;
    @SerializedName("delivery_person")
    @Expose
    private Object deliveryPerson;
    @SerializedName("delivery_date")
    @Expose
    private Object deliveryDate;
    @SerializedName("date")
    @Expose
    private String date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBAddress() {
        return bAddress;
    }

    public void setBAddress(String bAddress) {
        this.bAddress = bAddress;
    }

    public String getSAddress() {
        return sAddress;
    }

    public void setSAddress(String sAddress) {
        this.sAddress = sAddress;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getCart() {
        return cart;
    }

    public void setCart(String cart) {
        this.cart = cart;
    }

    public String getCartTotal() {
        return cartTotal;
    }

    public void setCartTotal(String cartTotal) {
        this.cartTotal = cartTotal;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getAmountReceived() {
        return amountReceived;
    }

    public void setAmountReceived(String amountReceived) {
        this.amountReceived = amountReceived;
    }

    public String getDuePayment() {
        return duePayment;
    }

    public void setDuePayment(String duePayment) {
        this.duePayment = duePayment;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPReference() {
        return pReference;
    }

    public void setPReference(String pReference) {
        this.pReference = pReference;
    }

    public String getOrderNote() {
        return orderNote;
    }

    public void setOrderNote(String orderNote) {
        this.orderNote = orderNote;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public String getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(String salesPerson) {
        this.salesPerson = salesPerson;
    }

    public Object getCancelNote() {
        return cancelNote;
    }

    public void setCancelNote(Object cancelNote) {
        this.cancelNote = cancelNote;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public Object getTracking() {
        return tracking;
    }

    public void setTracking(Object tracking) {
        this.tracking = tracking;
    }

    public Object getDeliveryPerson() {
        return deliveryPerson;
    }

    public void setDeliveryPerson(Object deliveryPerson) {
        this.deliveryPerson = deliveryPerson;
    }

    public Object getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Object deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}

