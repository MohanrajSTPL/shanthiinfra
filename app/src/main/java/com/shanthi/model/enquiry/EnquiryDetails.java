package com.shanthi.model.enquiry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EnquiryDetails {
    @SerializedName("cust_ind_id")
    @Expose
    private String custIndId;
    @SerializedName("custtype_id")
    @Expose
    private String customerTypeId;
    @SerializedName("cust_indv_fname")
    @Expose
    private String custIndvFname;
    @SerializedName("cust_indv_lname")
    @Expose
    private String custIndvLname;
    @SerializedName("cust_indv_mobile")
    @Expose
    private String custIndvMobile;
    @SerializedName("cust_indv_email")
    @Expose
    private String custIndvEmail;
    @SerializedName("cust_indv_gst_no")
    @Expose
    private String custIndvGstNo;
    @SerializedName("cust_indv_pan_no")
    @Expose
    private String custIndvPanNo;
    @SerializedName("cust_indv_opn_bal")
    @Expose
    private String custIndvOpnBal;
    @SerializedName("cust_indv_credit_lmt")
    @Expose
    private String custIndvCreditLmt;
    @SerializedName("cust_indv_duedays")
    @Expose
    private String custIndvDuedays;
    @SerializedName("cust_indv_paytype")
    @Expose
    private String custIndvPaytype;
    @SerializedName("meta_create_dts")
    @Expose
    private String metaCreateDts;
    @SerializedName("enquiry_id")
    @Expose
    private String enquiryId;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("enquiry_title")
    @Expose
    private String enquiryTitle;
    @SerializedName("enquiry_status_id")
    @Expose
    private String enquiryStatusId;
    @SerializedName("enquiry_date")
    @Expose
    private String enquiryDate;
    @SerializedName("item_id")
    @Expose
    private String itemId;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("min_quant")
    @Expose
    private String minQuant;
    @SerializedName("max_quant")
    @Expose
    private String maxQuant;
    @SerializedName("frquency")
    @Expose
    private String frquency;
    @SerializedName("enq_sts_id")
    @Expose
    private String enqStsId;
    @SerializedName("enq_sts_name")
    @Expose
    private String enqStsName;
    @SerializedName("enq_sts_descr")
    @Expose
    private String enqStsDescr;
    @SerializedName("prod_id")
    @Expose
    private String prodId;
    @SerializedName("prod_name")
    @Expose
    private String prodName;
    @SerializedName("prod_desc")
    @Expose
    private String prodDesc;
    @SerializedName("prod_quantity")
    @Expose
    private String prodQuantity;
    @SerializedName("prod_color")
    @Expose
    private String prodColor;
    @SerializedName("prod_price")
    @Expose
    private String prodPrice;
    @SerializedName("fld_freqid")
    @Expose
    private String fldFreqid;
    @SerializedName("fld_freqname")
    @Expose
    private String fldFreqname;

    @SerializedName("cust_comp_id")
    @Expose
    private String custCompId;
    @SerializedName("cust_comp_name")
    @Expose
    private String custCompName;
    @SerializedName("cust_comp_mob")
    @Expose
    private String custCompMob;
    @SerializedName("cnt_per_name")
    @Expose
    private String inchargeCompName;
    @SerializedName("cnt_per_mobile")
    @Expose
    private String inchargeMob;
    @SerializedName("incharge_designation")
    @Expose
    private String inchargeDesignation;
    @SerializedName("cust_comp_email")
    @Expose
    private String custCompEmail;
    @SerializedName("cust_comp_gst_no")
    @Expose
    private String custCompGstNo;
    @SerializedName("cust_comp_pan_no")
    @Expose
    private String custCompPanNo;
    @SerializedName("cust_comp_opn_bal")
    @Expose
    private String custCompOpnBal;
    @SerializedName("cust_comp_credit_lmt")
    @Expose
    private String custCompCreditLmt;
    @SerializedName("cust_comp_duedays")
    @Expose
    private String custCompDuedays;
    @SerializedName("cust_comp_paytype")
    @Expose
    private String custCompPaytype;
    @SerializedName("cust_comp_brnch")
    @Expose
    private String custCompBrnch;
    @SerializedName("cust_address")
    @Expose
    private String custAddress;
    @SerializedName("cust_city")
    @Expose
    private String custCity;
    @SerializedName("cust_state")
    @Expose
    private String custState;
    @SerializedName("cust_pincode")
    @Expose
    private String custPincode;
    @SerializedName("product")
    @Expose
    private List<Product> productsList;
    @SerializedName("lead_status_id")
    @Expose
    private String leadStatusId;
    @SerializedName("lead_source_id")
    @Expose
    private String leadSourceId;
    @SerializedName("lead_comm_id")
    @Expose
    private String leadCommunicationId;
    @SerializedName("lead_status_name")
    @Expose
    private String leadStatusName;
    @SerializedName("lead_source_name")
    @Expose
    private String leadSourceName;
    @SerializedName("lead_comm_name")
    @Expose
    private String leadCommunicationName;

    public String getCustIndId() {
        return custIndId;
    }

    public void setCustIndId(String custIndId) {
        this.custIndId = custIndId;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getCustIndvFname() {
        return custIndvFname;
    }

    public void setCustIndvFname(String custIndvFname) {
        this.custIndvFname = custIndvFname;
    }

    public String getCustIndvLname() {
        return custIndvLname;
    }

    public void setCustIndvLname(String custIndvLname) {
        this.custIndvLname = custIndvLname;
    }

    public String getCustIndvMobile() {
        return custIndvMobile;
    }

    public void setCustIndvMobile(String custIndvMobile) {
        this.custIndvMobile = custIndvMobile;
    }

    public String getCustIndvEmail() {
        return custIndvEmail;
    }

    public void setCustIndvEmail(String custIndvEmail) {
        this.custIndvEmail = custIndvEmail;
    }

    public String getCustIndvGstNo() {
        return custIndvGstNo;
    }

    public void setCustIndvGstNo(String custIndvGstNo) {
        this.custIndvGstNo = custIndvGstNo;
    }

    public String getCustIndvPanNo() {
        return custIndvPanNo;
    }

    public void setCustIndvPanNo(String custIndvPanNo) {
        this.custIndvPanNo = custIndvPanNo;
    }

    public String getCustIndvOpnBal() {
        return custIndvOpnBal;
    }

    public void setCustIndvOpnBal(String custIndvOpnBal) {
        this.custIndvOpnBal = custIndvOpnBal;
    }

    public String getCustIndvCreditLmt() {
        return custIndvCreditLmt;
    }

    public void setCustIndvCreditLmt(String custIndvCreditLmt) {
        this.custIndvCreditLmt = custIndvCreditLmt;
    }

    public String getCustIndvDuedays() {
        return custIndvDuedays;
    }

    public void setCustIndvDuedays(String custIndvDuedays) {
        this.custIndvDuedays = custIndvDuedays;
    }

    public String getCustIndvPaytype() {
        return custIndvPaytype;
    }

    public void setCustIndvPaytype(String custIndvPaytype) {
        this.custIndvPaytype = custIndvPaytype;
    }

    public String getMetaCreateDts() {
        return metaCreateDts;
    }

    public void setMetaCreateDts(String metaCreateDts) {
        this.metaCreateDts = metaCreateDts;
    }

    public String getEnquiryId() {
        return enquiryId;
    }

    public void setEnquiryId(String enquiryId) {
        this.enquiryId = enquiryId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getEnquiryTitle() {
        return enquiryTitle;
    }

    public void setEnquiryTitle(String enquiryTitle) {
        this.enquiryTitle = enquiryTitle;
    }

    public String getEnquiryStatusId() {
        return enquiryStatusId;
    }

    public void setEnquiryStatusId(String enquiryStatusId) {
        this.enquiryStatusId = enquiryStatusId;
    }

    public String getEnquiryDate() {
        return enquiryDate;
    }

    public void setEnquiryDate(String enquiryDate) {
        this.enquiryDate = enquiryDate;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getMinQuant() {
        return minQuant;
    }

    public void setMinQuant(String minQuant) {
        this.minQuant = minQuant;
    }

    public String getMaxQuant() {
        return maxQuant;
    }

    public void setMaxQuant(String maxQuant) {
        this.maxQuant = maxQuant;
    }

    public String getFrquency() {
        return frquency;
    }

    public void setFrquency(String frquency) {
        this.frquency = frquency;
    }

    public String getEnqStsId() {
        return enqStsId;
    }

    public void setEnqStsId(String enqStsId) {
        this.enqStsId = enqStsId;
    }

    public String getEnqStsName() {
        return enqStsName;
    }

    public void setEnqStsName(String enqStsName) {
        this.enqStsName = enqStsName;
    }

    public String getEnqStsDescr() {
        return enqStsDescr;
    }

    public void setEnqStsDescr(String enqStsDescr) {
        this.enqStsDescr = enqStsDescr;
    }

    public String getProdId() {
        return prodId;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public String getProdDesc() {
        return prodDesc;
    }

    public void setProdDesc(String prodDesc) {
        this.prodDesc = prodDesc;
    }

    public String getProdQuantity() {
        return prodQuantity;
    }

    public void setProdQuantity(String prodQuantity) {
        this.prodQuantity = prodQuantity;
    }

    public String getProdColor() {
        return prodColor;
    }

    public void setProdColor(String prodColor) {
        this.prodColor = prodColor;
    }

    public String getProdPrice() {
        return prodPrice;
    }

    public void setProdPrice(String prodPrice) {
        this.prodPrice = prodPrice;
    }

    public String getFldFreqid() {
        return fldFreqid;
    }

    public void setFldFreqid(String fldFreqid) {
        this.fldFreqid = fldFreqid;
    }

    public String getFldFreqname() {
        return fldFreqname;
    }

    public void setFldFreqname(String fldFreqname) {
        this.fldFreqname = fldFreqname;
    }

    public String getCustCompId() {
        return custCompId;
    }

    public void setCustCompId(String custCompId) {
        this.custCompId = custCompId;
    }

    public String getCustCompName() {
        return custCompName;
    }

    public void setCustCompName(String custCompName) {
        this.custCompName = custCompName;
    }

    public String getCustCompMob() {
        return custCompMob;
    }

    public void setCustCompMob(String custCompMob) {
        this.custCompMob = custCompMob;
    }

    public String getInchargeCompName() {
        return inchargeCompName;
    }

    public void setInchargeCompName(String inchargeCompName) {
        this.inchargeCompName = inchargeCompName;
    }

    public String getInchargeMob() {
        return inchargeMob;
    }

    public void setInchargeMob(String inchargeMob) {
        this.inchargeMob = inchargeMob;
    }

    public String getInchargeDesignation() {
        return inchargeDesignation;
    }

    public void setInchargeDesignation(String inchargeDesignation) {
        this.inchargeDesignation = inchargeDesignation;
    }

    public String getCustCompEmail() {
        return custCompEmail;
    }

    public void setCustCompEmail(String custCompEmail) {
        this.custCompEmail = custCompEmail;
    }

    public String getCustCompGstNo() {
        return custCompGstNo;
    }

    public void setCustCompGstNo(String custCompGstNo) {
        this.custCompGstNo = custCompGstNo;
    }

    public String getCustCompPanNo() {
        return custCompPanNo;
    }

    public void setCustCompPanNo(String custCompPanNo) {
        this.custCompPanNo = custCompPanNo;
    }

    public String getCustCompOpnBal() {
        return custCompOpnBal;
    }

    public void setCustCompOpnBal(String custCompOpnBal) {
        this.custCompOpnBal = custCompOpnBal;
    }

    public String getCustCompCreditLmt() {
        return custCompCreditLmt;
    }

    public void setCustCompCreditLmt(String custCompCreditLmt) {
        this.custCompCreditLmt = custCompCreditLmt;
    }

    public String getCustCompDuedays() {
        return custCompDuedays;
    }

    public void setCustCompDuedays(String custCompDuedays) {
        this.custCompDuedays = custCompDuedays;
    }

    public String getCustCompPaytype() {
        return custCompPaytype;
    }

    public void setCustCompPaytype(String custCompPaytype) {
        this.custCompPaytype = custCompPaytype;
    }

    public String getCustCompBrnch() {
        return custCompBrnch;
    }

    public void setCustCompBrnch(String custCompBrnch) {
        this.custCompBrnch = custCompBrnch;
    }

    public String getCustAddress() {
        return custAddress;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    public String getCustCity() {
        return custCity;
    }

    public void setCustCity(String custCity) {
        this.custCity = custCity;
    }

    public String getCustState() {
        return custState;
    }

    public void setCustState(String custState) {
        this.custState = custState;
    }

    public String getCustPincode() {
        return custPincode;
    }

    public void setCustPincode(String custPincode) {
        this.custPincode = custPincode;
    }

    public List<Product> getProductsList() {
        return productsList;
    }

    public void setProductsList(List<Product> productsList) {
        this.productsList = productsList;
    }

    public String getLeadStatusId() {
        return leadStatusId;
    }

    public void setLeadStatusId(String leadStatusId) {
        this.leadStatusId = leadStatusId;
    }

    public String getLeadSourceId() {
        return leadSourceId;
    }

    public void setLeadSourceId(String leadSourceId) {
        this.leadSourceId = leadSourceId;
    }

    public String getLeadCommunicationId() {
        return leadCommunicationId;
    }

    public void setLeadCommunicationId(String leadCommunicationId) {
        this.leadCommunicationId = leadCommunicationId;
    }

    public String getLeadStatusName() {
        return leadStatusName;
    }

    public void setLeadStatusName(String leadStatusName) {
        this.leadStatusName = leadStatusName;
    }

    public String getLeadSourceName() {
        return leadSourceName;
    }

    public void setLeadSourceName(String leadSourceName) {
        this.leadSourceName = leadSourceName;
    }

    public String getLeadCommunicationName() {
        return leadCommunicationName;
    }

    public void setLeadCommunicationName(String leadCommunicationName) {
        this.leadCommunicationName = leadCommunicationName;
    }
}