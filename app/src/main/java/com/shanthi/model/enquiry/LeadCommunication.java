package com.shanthi.model.enquiry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeadCommunication {

    @SerializedName("lead_comm_id")
    @Expose
    String leadCommunicationId;
    @SerializedName("lead_comm_mode_name")
    @Expose
    String leadCommunicationName;

    public String getLeadCommunicationId() {
        return leadCommunicationId;
    }

    public void setLeadCommunicationId(String leadCommunicationId) {
        this.leadCommunicationId = leadCommunicationId;
    }

    public String getLeadCommunicationName() {
        return leadCommunicationName;
    }

    public void setLeadCommunicationName(String leadCommunicationName) {
        this.leadCommunicationName = leadCommunicationName;
    }
}