package com.shanthi.model.enquiry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeadSource {

    @SerializedName("lead_src_id")
    @Expose
    String leadSourceId;
    @SerializedName("lead_src_name")
    @Expose
    String leadSourceName;

    public String getLeadSourceId() {
        return leadSourceId;
    }

    public void setLeadSourceId(String leadSourceId) {
        this.leadSourceId = leadSourceId;
    }

    public String getLeadSourceName() {
        return leadSourceName;
    }

    public void setLeadSourceName(String leadSourceName) {
        this.leadSourceName = leadSourceName;
    }
}
