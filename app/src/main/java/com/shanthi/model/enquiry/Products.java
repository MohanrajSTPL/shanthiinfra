package com.shanthi.model.enquiry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Products {

    @SerializedName("ITEM_NAME")
    @Expose
    String product;
    @SerializedName("ITEM_ID")
    @Expose
    String productId;
    @SerializedName("UNIT")
    @Expose
    String unit;
    @SerializedName("UNIT_ID")
    @Expose
    String unitId;
    @SerializedName("MIN_REQUIREMENT")
    @Expose
    String minRequirement;
    @SerializedName("MAX_REQUIREMENT")
    @Expose
    String maxRequirement;
    @SerializedName("FREQUENCY")
    @Expose
    String frequency;
    @SerializedName("FREQUENCY_ID")
    @Expose
    String frequencyId;
    @SerializedName("ENQUIRY_STATUS")
    @Expose
    String enquiryStatus;
    @SerializedName("ENQUIRY_ID")
    @Expose
    String enquiryId;
    @SerializedName("UNIT_MEASUREMENT")
    @Expose
    String unitMeasurement;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getMinRequirement() {
        return minRequirement;
    }

    public void setMinRequirement(String minRequirement) {
        this.minRequirement = minRequirement;
    }

    public String getMaxRequirement() {
        return maxRequirement;
    }

    public void setMaxRequirement(String maxRequirement) {
        this.maxRequirement = maxRequirement;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getFrequencyId() {
        return frequencyId;
    }

    public void setFrequencyId(String frequencyId) {
        this.frequencyId = frequencyId;
    }

    public String getEnquiryStatus() {
        return enquiryStatus;
    }

    public void setEnquiryStatus(String enquiryStatus) {
        this.enquiryStatus = enquiryStatus;
    }

    public String getEnquiryId() {
        return enquiryId;
    }

    public void setEnquiryId(String enquiryId) {
        this.enquiryId = enquiryId;
    }

    public String getUnitMeasurement() {
        return unitMeasurement;
    }

    public void setUnitMeasurement(String unitMeasurement) {
        this.unitMeasurement = unitMeasurement;
    }
}
