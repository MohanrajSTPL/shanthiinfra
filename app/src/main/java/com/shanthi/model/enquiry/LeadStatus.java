package com.shanthi.model.enquiry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeadStatus {

    @SerializedName("lead_sts_id")
    @Expose
    String leadStatusId;
    @SerializedName("lead_sts_name")
    @Expose
    String leadStatusName;

    public String getLeadStatusId() {
        return leadStatusId;
    }

    public void setLeadStatusId(String leadStatusId) {
        this.leadStatusId = leadStatusId;
    }

    public String getLeadStatusName() {
        return leadStatusName;
    }

    public void setLeadStatusName(String leadStatusName) {
        this.leadStatusName = leadStatusName;
    }
}
