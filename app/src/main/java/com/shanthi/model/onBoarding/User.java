package com.shanthi.model.onBoarding;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {


    @SerializedName("Userttype")
    @Expose
    private String userttype;
    @SerializedName("Sessionid")
    @Expose
    private String sessionid;
    @SerializedName("usertype")
    @Expose
    private String usertype;

    public String getUserttype() {
        return userttype;
    }

    public void setUserttype(String userttype) {
        this.userttype = userttype;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

}
