package com.shanthi.model.shanthiInfra;

public class Contractor {

    String siteid;
    String vendorid;
    String workid;
    String Tdate;
    String amount;
    String descr;
    String sessionid;
    public String getSiteId() {
        return siteid;
    }

    public void setSiteId(String siteid) {
        this.siteid = siteid;
    }

    public String getVendorid() {
        return vendorid;
    }

    public void setVendorid(String vendorid) {
        this.vendorid = vendorid;
    }
    public String getworkid() {
        return workid;
    }

    public void setworkid(String workid) {
        this.workid = workid;
    }

    public String getTdate() {
        return Tdate;
    }

    public void setTdate(String Tdate) {
        this.Tdate = Tdate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return descr;
    }

    public void setDescription(String descr) {
        this.descr = descr;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }
}
