package com.shanthi.model.shanthiInfra;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sites {

    @SerializedName("Site_Id")
    @Expose
    private String siteId;
    @SerializedName("Site_Name")
    @Expose
    private String siteName;
    @SerializedName("CustomerId")
    @Expose
    private String customerId;

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
