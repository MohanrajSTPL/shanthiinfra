package com.shanthi.model.shanthiInfra;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PL {

    @SerializedName("SiteName")
    @Expose
    private String siteName;
    @SerializedName("Contractor")
    @Expose
    private String contractor;
    @SerializedName("SiteValue")
    @Expose
    private String siteValue;
    @SerializedName("Cpayment")
    @Expose
    private String cpayment;
    @SerializedName("SiteExpense")
    @Expose
    private String siteExpense;
    @SerializedName("GrandTotal")
    @Expose
    private String grand;

    @SerializedName("Net")
    @Expose
    private String netTotal;

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getContractor() {
        return contractor;
    }

    public void setContractor(String contractor) {
        this.contractor = contractor;
    }

    public String getSiteValue() {
        return siteValue;
    }

    public void setSiteValue(String siteValue) {
        this.siteValue = siteValue;
    }

    public String getCpayment() {
        return cpayment;
    }

    public void setCpayment(String cpayment) {
        this.cpayment = cpayment;
    }

    public String getSiteExpense() {
        return siteExpense;
    }

    public void setSiteExpense(String siteExpense) {
        this.siteExpense = siteExpense;
    }

    public String getGrand() {
        return grand;
    }

    public void setGrand(String grand) {
        this.grand = grand;
    }
    public String getNetTotal() {
        return netTotal;
    }

    public void setNetTotal(String netTotal) {
        this.netTotal = netTotal;
    }

}
