package com.shanthi.model.shanthiInfra;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Work {


    @SerializedName("workid")
    @Expose
    private String workid;
    @SerializedName("workname")
    @Expose
    private String workname;

    public String getWorkid() {
        return workid;
    }

    public void setWorkid(String workid) {
        this.workid = workid;
    }

    public String getWorkname() {
        return workname;
    }

    public void setWorkname(String workname) {
        this.workname = workname;
    }

}
