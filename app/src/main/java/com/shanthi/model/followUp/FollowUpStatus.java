package com.shanthi.model.followUp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FollowUpStatus {

    @SerializedName("lead_sts_id")
    @Expose
    String followUpStatusId;
    @SerializedName("lead_sts_name")
    @Expose
    String followUpStatusName;

    public String getFollowUpStatusId() {
        return followUpStatusId;
    }

    public void setFollowUpStatusId(String followUpStatusId) {
        this.followUpStatusId = followUpStatusId;
    }

    public String getFollowUpStatusName() {
        return followUpStatusName;
    }

    public void setFollowUpStatusName(String followUpStatusName) {
        this.followUpStatusName = followUpStatusName;
    }
}