package com.shanthi.model.followUp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FollowUpDetails {

    @SerializedName("enquiry_id")
    @Expose
    String enquiryId;
    @SerializedName("followup_id")
    @Expose
    String followUpId;
    @SerializedName("followdate")
    @Expose
    String followUpDate;
    @SerializedName("comments")
    @Expose
    String comments;
    @SerializedName("lead_sts_name")
    @Expose
    String followUpStatus;
    @SerializedName("lead_sts_id")
    @Expose
    String followUpStatusId;

    public String getEnquiryId() {
        return enquiryId;
    }

    public void setEnquiryId(String enquiryId) {
        this.enquiryId = enquiryId;
    }

    public String getFollowUpId() {
        return followUpId;
    }

    public void setFollowUpId(String followUpId) {
        this.followUpId = followUpId;
    }

    public String getFollowUpDate() {
        return followUpDate;
    }

    public void setFollowUpDate(String followUpDate) {
        this.followUpDate = followUpDate;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getFollowUpStatus() {
        return followUpStatus;
    }

    public void setFollowUpStatus(String followUpStatus) {
        this.followUpStatus = followUpStatus;
    }

    public String getFollowUpStatusId() {
        return followUpStatusId;
    }

    public void setFollowUpStatusId(String followUpStatusId) {
        this.followUpStatusId = followUpStatusId;
    }
}