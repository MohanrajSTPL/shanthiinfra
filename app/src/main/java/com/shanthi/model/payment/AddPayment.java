package com.shanthi.model.payment;

public class AddPayment {

    String siteid;
    String ptype;
    String pid;
    String pdate;
    String amount;
    String descr;
    String sessionid;
    public String getSiteId() {
        return siteid;
    }

    public void setSiteId(String siteid) {
        this.siteid = siteid;
    }

    public String getPtype() {
        return ptype;
    }

    public void setPtype(String ptype) {
        this.ptype = ptype;
    }
    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getTdate() {
        return pdate;
    }

    public void setTdate(String pdate) {
        this.pdate = pdate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return descr;
    }

    public void setDescription(String descr) {
        this.descr = descr;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }
}
