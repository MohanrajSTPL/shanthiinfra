package com.shanthi.presenter.onBoarding;

import com.shanthi.service.onBoarding.OnBoardingService;
import com.shanthi.ui.activity.onBoarding.ChangePasswordActivity;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

import static com.shanthi.utils.AppUtils.checkThrowable;

public class ChangePasswordPresenter extends RxPresenter<ChangePasswordActivity> {
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void changePassword(String userId,String oldPassword, String newPassword) {
        compositeDisposable.add(OnBoardingService.changePassword(userId,oldPassword, newPassword)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    if (response.getResponseCode() == 1)
                        view.onGetChangePasswordResponseSuccess(response);
                    else
                        view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null)
            compositeDisposable.clear();
    }
}
