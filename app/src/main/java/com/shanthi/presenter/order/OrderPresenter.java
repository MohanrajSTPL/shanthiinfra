package com.shanthi.presenter.order;

import com.shanthi.model.shanthiInfra.Contractor;
import com.shanthi.service.order.OrderService;
import com.shanthi.ui.activity.order.OrderActivity;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

import static com.shanthi.utils.AppUtils.checkThrowable;

public class OrderPresenter extends RxPresenter<OrderActivity> {
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void getVendor( ) {
        compositeDisposable.add(OrderService.getVendor()
                .compose(deliverFirst())
                 .subscribe(delivery -> delivery.split((view, response) -> {
                   // if (response.getResponseCode() == 1)
                        view.onExpenseTypeSuccess(response);
                 //   else
                      //  view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }

    public void getSite() {
        compositeDisposable.add(OrderService.getSite()
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    // if (response.getResponseCode() == 1)
                    view.onSiteResponseSuccess(response);
                    //   else
                    //  view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }
    public void getJob() {
        compositeDisposable.add(OrderService.getJob()
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    // if (response.getResponseCode() == 1)
                    view.onJobResponseSuccess(response);
                    //   else
                    //  view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }



    public void addContractor(Contractor newOrderItemDetails) {
        compositeDisposable.add(OrderService.addContractor(newOrderItemDetails)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                 //   if (response.getResponseCode() == 1)
                        view.onAddExpenseResponseSuccess(response);
                  //  else
                        view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }

    public void addPayment(Contractor newOrderItemDetails) {
        compositeDisposable.add(OrderService.addContractor(newOrderItemDetails)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    //   if (response.getResponseCode() == 1)
                    view.onAddExpenseResponseSuccess(response);
                    //  else
                    view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null)
            compositeDisposable.clear();
    }
}