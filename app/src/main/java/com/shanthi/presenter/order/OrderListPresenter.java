package com.shanthi.presenter.order;

import com.shanthi.service.order.OrderService;
import com.shanthi.ui.activity.order.PendingApproval;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

import static com.shanthi.utils.AppUtils.checkThrowable;

public class OrderListPresenter extends RxPresenter<PendingApproval> {
    String res = "1";
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
//
//    public void getPendingApproval(Expense expense) {
//        compositeDisposable.add(OrderService.getPendingApproval(expense)
//                .compose(deliverFirst())
//                .subscribe(delivery -> delivery.split((view, response) -> {
//                //    if (response.getResponseCode() == 1)
//                        view.onPendingListResponseSuccess(response);
//               //     else
//                   //     view.onFailure(response);
//                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
//    }
    public void getPendingApproval( ) {
        compositeDisposable.add(OrderService.getPendingApproval()
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                //    if (response.getResponseCode() == 1)
                        view.onPendingListResponseSuccess(response);
               //     else
                   //     view.onFailure(response);
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }



    public void sendApproval(String expId,String operation) {
        compositeDisposable.add(OrderService.sendApproval(expId,operation)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {

                     if (response.getResponseCode().equals("1") )
                     view.onSendResponseSuccess(response);
                     else
                     view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }
    public void declineExpense(String expId,String operation) {
        compositeDisposable.add(OrderService.declineExpense(expId,operation)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {

                     if (response.getResponseCode().equals("1") )
                     view.onDeclineResponseSuccess(response);
                     else
                     view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null)
            compositeDisposable.clear();
    }
}
