package com.shanthi.presenter.order;

import com.shanthi.ui.activity.order.OrderAddCustomerActivity;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

public class OrderAddCustomerPresenter extends RxPresenter<OrderAddCustomerActivity> {
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

//    public void addCustomerCompanyDetails(boolean isCompany, String companyName, String inchargeName, String inchargeDestination, String inchargeMobileNumber, String companyMobileNumber,
//                                          String branch, String address, String city, String state, String pinCode, String gst, String pan, String customerName, String customerMobileNumber, String customerAddress,
//                                          String customerCity, String customerState, String customerPincode, String customerPAN) {
//        compositeDisposable.add(EnquiryService.addCustomerCompanyDetails(isCompany, companyName, inchargeName, inchargeDestination,
//                inchargeMobileNumber, companyMobileNumber, branch, address, city, state, pinCode, gst, pan,
//                customerName, customerMobileNumber, customerAddress, customerCity, customerState, customerPincode,
//                customerPAN)
//                .compose(deliverFirst())
//                .subscribe(delivery -> delivery.split((view, response) -> {
//                    if (response.getResponseCode() == 1)
//                        view.onAddCustomerCompanyDetailsResponseSuccess(response);
//                    else
//                        view.onFailure(response.getMessage());
//                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null)
            compositeDisposable.clear();
    }
}
