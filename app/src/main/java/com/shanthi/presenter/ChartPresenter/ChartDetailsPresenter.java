package com.shanthi.presenter.ChartPresenter;

import com.shanthi.model.construction.Expense;
import com.shanthi.service.chart.ChartService;
import com.shanthi.ui.activity.home.HomeActivity;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

import static com.shanthi.utils.AppUtils.checkThrowable;

public class ChartDetailsPresenter extends RxPresenter<HomeActivity> {
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void getChartDetails(Expense expense) {
        compositeDisposable.add(ChartService.getChartDetails(expense)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {

                    // if (response.getResponseCode() == 1)
                    view.onChartDetailResponseSuccess(response);
                    // else
                    view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }

    public void getOnclickChartdetails(Expense expense) {
        compositeDisposable.add(ChartService.getOnclickChartdetails(expense)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {

                     if (response.getResponseCode().equals("1") )
                    view.onClickChartDetailResponseSuccess(response);
                     else
                    view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }
}
