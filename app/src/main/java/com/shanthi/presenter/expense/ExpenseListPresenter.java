package com.shanthi.presenter.expense;

import com.shanthi.model.construction.Expense;
import com.shanthi.service.order.OrderService;
import com.shanthi.ui.activity.order.ExpenseListActivity;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

import static com.shanthi.utils.AppUtils.checkThrowable;

public class ExpenseListPresenter extends RxPresenter<ExpenseListActivity> {
    String res = "1";
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void getMyExpenseList(Expense sessionId) {
        compositeDisposable.add(OrderService.getMyExpenseList(sessionId)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    if (response.getResponseCode().equals("1"))
                        view.onUserExpenseListResponseSuccess(response);
                    else
                        view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }
}
