package com.shanthi.presenter.expense;

import com.shanthi.model.construction.Construction;
import com.shanthi.service.order.OrderService;
import com.shanthi.ui.activity.order.ExpenseActivity;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

import static com.shanthi.utils.AppUtils.checkThrowable;

public class NewExpensePresenter extends RxPresenter<ExpenseActivity> {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    public void getExpenses(String type) {
        compositeDisposable.add(OrderService.getExpenses(type)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    // if (response.getResponseCode() == 1)
                    view.onExpenseTypeSuccess(response);
                    //   else
                    //  view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }

    public void getSiteDropdown(String type) {
        compositeDisposable.add(OrderService.getSiteDropdown(type)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    // if (response.getResponseCode() == 1)
                    view.onSiteDropdownResponseSuccess(response);
                    //   else
                    //  view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }
//    public void getSite( ) {
//        compositeDisposable.add(OrderService.getSite()
//                .compose(deliverFirst())
//                .subscribe(delivery -> delivery.split((view, response) -> {
//                    // if (response.getResponseCode() == 1)
//                    view.onSiteResponseSuccess(response);
//                    //   else
//                    //  view.onFailure(response.getMessage());
//                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
//    }



    public void addExpense(Construction newOrderItemDetails) {
        compositeDisposable.add(OrderService.addExpense(newOrderItemDetails)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    //   if (response.getResponseCode() == 1)
                    view.onAddExpenseResponseSuccess(response);
                    //  else
                    view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null)
            compositeDisposable.clear();
    }
}
