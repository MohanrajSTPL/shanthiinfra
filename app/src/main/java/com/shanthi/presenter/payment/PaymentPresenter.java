package com.shanthi.presenter.payment;

import com.shanthi.model.payment.AddPayment;
import com.shanthi.service.order.OrderService;
import com.shanthi.ui.activity.payment.PaymentActivity;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

import static com.shanthi.utils.AppUtils.checkThrowable;

public class PaymentPresenter extends RxPresenter<PaymentActivity> {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void addContractor(AddPayment newOrderItemDetails) {
        compositeDisposable.add(OrderService.addPayment(newOrderItemDetails)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    //   if (response.getResponseCode() == 1)
                    view.onAddPaymentResponseSuccess(response);
                    //  else
                    view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }

    public void getSite() {
        compositeDisposable.add(OrderService.getSite()
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    // if (response.getResponseCode() == 1)
                    view.onSiteResponseSuccess(response);
                    //   else
                    //  view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }
    public void getPayment() {
        compositeDisposable.add(OrderService.getPayment()
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    // if (response.getResponseCode() == 1)
                    view.onPaymentResponseSuccess(response);
                    //   else
                    //  view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }


}
