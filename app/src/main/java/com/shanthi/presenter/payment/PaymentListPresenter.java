package com.shanthi.presenter.payment;

import com.shanthi.service.order.OrderService;
import com.shanthi.ui.activity.payment.PaymentListActivity;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

import static com.shanthi.utils.AppUtils.checkThrowable;

public class PaymentListPresenter extends RxPresenter<PaymentListActivity> {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void getMyPaymentList( ) {
        compositeDisposable.add(OrderService.getMyPaymentList()
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                   if (response.getMessage().equals("success"))
                       view.onPaymentListResponseSuccess(response);
                    else
                        view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }
}
