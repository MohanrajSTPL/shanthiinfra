package com.shanthi.presenter.sitestatus;

import com.shanthi.service.order.OrderService;
import com.shanthi.ui.activity.sitestatus.SiteStatusActivity;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

import static com.shanthi.utils.AppUtils.checkThrowable;

public class SiteStatusPresenter extends RxPresenter<SiteStatusActivity> {
    String res = "1";
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void getSiteStatus( ) {
        compositeDisposable.add(OrderService.getSiteStatus()
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    //    if (response.getResponseCode() == 1)
                    view.onSiteStatusResponseSuccess(response);
                    //     else
                    //     view.onFailure(response);
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }

    public void updateStatus(String siteId, String item) {
        compositeDisposable.add(OrderService.updateStatus(siteId,item)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    //   if (response.getResponseCode() == 1)
                    view.onUpdateStatusSuccess(response);
                    //  else
                    view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }

}
