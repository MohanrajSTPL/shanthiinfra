package com.shanthi.presenter;

import android.content.Context;

import com.shanthi.application.SandApp;

import nucleus5.presenter.RxPresenter;

import static com.shanthi.utils.LogUtils.makeLogTag;

public class BasePresenter<View> extends RxPresenter<View> {
    protected static final String TAG = makeLogTag("BasePresenter");

    protected Context getContext() {
        return SandApp.getInstance().getApplicationContext();
    }
}

