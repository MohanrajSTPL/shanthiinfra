package com.shanthi.presenter.PL;

import com.shanthi.service.chart.ChartService;
import com.shanthi.ui.activity.order.PLActivity;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

import static com.shanthi.utils.AppUtils.checkThrowable;

public class PLPresenter extends RxPresenter<PLActivity> {
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void getPLList( ) {
        compositeDisposable.add(ChartService.getPLList()
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {

                    // if (response.getResponseCode() == 1)
                    view.onPLResponseSuccess(response);
                    // else
                    view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }

}
