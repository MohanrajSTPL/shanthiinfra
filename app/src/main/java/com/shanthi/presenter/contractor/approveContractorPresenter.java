package com.shanthi.presenter.contractor;

import com.shanthi.service.order.OrderService;
import com.shanthi.ui.activity.order.ApproveContractor;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

import static com.shanthi.utils.AppUtils.checkThrowable;

public class approveContractorPresenter extends RxPresenter<ApproveContractor> {

    String res = "1";
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void getPendingContractor( ) {
        compositeDisposable.add(OrderService.getPendingContractor()
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    //    if (response.getResponseCode() == 1)
                    view.onPendingContractorListResponseSuccess(response);
                    //     else
                    //     view.onFailure(response);
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }

    public void sendContractorApproval(String expId,String operation) {
        compositeDisposable.add(OrderService.sendContractorApproval(expId,operation)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {

                    if (response.getResponseCode().equals("1") )
                        view.onSendResponseSuccess(response);
                    else
                        view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null)
            compositeDisposable.clear();
    }
}
