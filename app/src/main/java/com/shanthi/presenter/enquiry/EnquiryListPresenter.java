//package com.bass.presenter.enquiry;
//
//import com.bass.service.enquiry.EnquiryService;
//import com.bass.ui.activity.enquiry.EnquiryListActivity;
//
//import io.reactivex.disposables.CompositeDisposable;
//import nucleus5.presenter.RxPresenter;
//
//import static com.bass.utils.AppUtils.checkThrowable;
//
//public class EnquiryListPresenter extends RxPresenter<EnquiryListActivity> {
//    private CompositeDisposable compositeDisposable = new CompositeDisposable();
//
//    public void getEnquiryList() {
//        compositeDisposable.add(EnquiryService.getEnquiryList()
//                .compose(deliverFirst())
//                .subscribe(delivery -> delivery.split((view, response) -> {
//                    if (response.getResponseCode() == 1)
//                        view.onEnquiryListResponseSuccess(response);
//                    else
//                        view.onFailure(response.getMessage());
//                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
//    }
//
//    public void searchEnquiryList(String searchKey) {
//        compositeDisposable.add(EnquiryService.searchEnquiryList(searchKey)
//                .compose(deliverFirst())
//                .subscribe(delivery -> delivery.split((view, response) -> {
//                    if (response.getResponseCode() == 1)
//                        view.onEnquiryListResponseSuccess(response);
//                    else
//                        view.onFailure(response.getMessage());
//                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        if (compositeDisposable != null)
//            compositeDisposable.clear();
//    }
//}
