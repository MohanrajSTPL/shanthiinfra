package com.shanthi.presenter.enquiry;

import com.shanthi.service.enquiry.EnquiryService;
import com.shanthi.ui.activity.enquiry.NewCustomerActivity;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

import static com.shanthi.utils.AppUtils.checkThrowable;

public class EnquiryAddCustomerPresenter extends RxPresenter<NewCustomerActivity> {
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void saveCustomer( String customerName, String phone,String email, String b_address) {
        compositeDisposable.add(EnquiryService.saveCustomer(customerName,phone,email,b_address)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    if (response.getResponseCode() == 1)
                        view.onAddCustomerSuccess(response);
                    else
                        view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null)
            compositeDisposable.clear();
    }
}
