//package com.bass.presenter.enquiry;
//
//import com.bass.service.enquiry.EnquiryService;
//import com.bass.ui.activity.enquiry.EnquiryDetailsActivity;
//
//import io.reactivex.disposables.CompositeDisposable;
//import nucleus5.presenter.RxPresenter;
//
//import static com.bass.utils.AppUtils.checkThrowable;
//
//public class EnquiryDetailsPresenter extends RxPresenter<EnquiryDetailsActivity> {
//    private CompositeDisposable compositeDisposable = new CompositeDisposable();
//
//    public void getEnquiryDetails(String enquiryId, String customerTypeId) {
//        compositeDisposable.add(EnquiryService.getEnquiryDetails(enquiryId, customerTypeId)
//                .compose(deliverFirst())
//                .subscribe(delivery -> delivery.split((view, response) -> {
//                    if (response.getResponseCode() == 1)
//                        view.onEnquiryDetailsResponseSuccess(response);
//                    else
//                        view.onFailure(response.getMessage());
//                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        if (compositeDisposable != null)
//            compositeDisposable.clear();
//    }
//}
