package com.shanthi.ui.fragment.dialogFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.shanthi.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class OrderAddCustomerFragment extends DialogFragment {
    @BindView(R.id.llCompany)
    LinearLayout llCompany;
    @BindView(R.id.llIndividual)
    LinearLayout llIndividual;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.rbCompany)
    RadioButton rbCompany;
    @BindView(R.id.rbIndividual)
    RadioButton rbIndividual;
    @BindView(R.id.etCompanyName)
    EditText etCompanyName;
    @BindView(R.id.etInchargeName)
    EditText etInchargeName;
    @BindView(R.id.etInchargeDesignation)
    EditText etInchargeDesignation;
    @BindView(R.id.etInchargeMobileNumber)
    EditText etInchargeMobileNumber;
    @BindView(R.id.etCompanyMobileNumber)
    EditText etCompanyMobileNumber;
    @BindView(R.id.etBranch)
    EditText etBranch;
    @BindView(R.id.etAddress)
    EditText etAddress;
    @BindView(R.id.etCity)
    EditText etCity;
    @BindView(R.id.etState)
    EditText etState;
    @BindView(R.id.etPincode)
    EditText etPincode;
    @BindView(R.id.etGST)
    EditText etGST;
    @BindView(R.id.etPAN)
    EditText etPAN;

    // individual
    @BindView(R.id.etCustomerName)
    EditText etCustomerName;
    @BindView(R.id.etCustomerMobileNumber)
    EditText etCustomerMobileNumber;
    @BindView(R.id.etCustomerAddress)
    EditText etCustomerAddress;
    @BindView(R.id.etCustomerCity)
    EditText etCustomerCity;
    @BindView(R.id.etCustomerState)
    EditText etCustomerState;
    @BindView(R.id.etCustomerPincode)
    EditText etCustomerPincode;
    @BindView(R.id.etCustomerPAN)
    EditText etCustomerPAN;
    private boolean isCompany = true;

    private Unbinder viewUnBinder;
    private Listener listener;

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public OrderAddCustomerFragment() {
    }

    public static OrderAddCustomerFragment newInstance() {
        Bundle args = new Bundle();
        OrderAddCustomerFragment fragment = new OrderAddCustomerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.order_add_customer_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewUnBinder = ButterKnife.bind(this, view);
        setCancelable(true);
        setRadioButtonClickListener();
    }

    private void setRadioButtonClickListener() {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                RadioButton rb = (RadioButton) radioGroup.findViewById(checkedId);
                if (rb.getText().toString().equalsIgnoreCase(getString(R.string.company))) {
                    llIndividual.setVisibility(View.GONE);
                    llCompany.setVisibility(View.VISIBLE);
                    isCompany = true;
                } else if (rb.getText().toString().equalsIgnoreCase(getString(R.string.individual_customer))) {
                    llCompany.setVisibility(View.GONE);
                    llIndividual.setVisibility(View.VISIBLE);
                    isCompany = false;
                }
            }
        });
    }

    @OnClick(R.id.tvSubmit)
    public void onSubmitClicked() {

        if (isCompany && etCompanyName.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_company_name));
            return;
        }
        if (isCompany && etInchargeName.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_incharge_name));
            return;
        }
        if (isCompany && etInchargeDesignation.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_incharge_designation));
            return;
        }
        if (isCompany && etInchargeMobileNumber.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_incharge_mobile_number));
            return;
        }
        /*if (isCompany && etCompanyMobileNumber.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_company_mobile_number));
            return;
        }
        if (isCompany && etBranch.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_branch));
            return;
        }
        if (isCompany && etAddress.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_address));
            return;
        }*/
        if (isCompany && etCity.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_city));
            return;
        }
        /*if (isCompany && etState.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_state));
            return;
        }
        if (isCompany && etPincode.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_pincode));
            return;
        }
        if (isCompany && etGST.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_gst));
            return;
        }
        if (isCompany && etPAN.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_pan));
            return;
        }*/

        // individual
        if (!isCompany && etCustomerName.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_customer_name));
            return;
        }
        if (!isCompany && etCustomerMobileNumber.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_mobile_number));
            return;
        }
        /*if (!isCompany && etCustomerAddress.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_address));
            return;
        }*/
        if (!isCompany && etCustomerCity.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_city));
            return;
        }
        /*if (!isCompany && etCustomerState.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_state));
            return;
        }
        if (!isCompany && etCustomerPincode.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_pincode));
            return;
        }

        if (!isCompany && etCustomerPAN.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_pan));
            return;
        }*/

        // company
        String companyName = etCompanyName.getText().toString().trim();
        String inchargeName = etInchargeName.getText().toString().trim();
        String inchargeDestination = etInchargeDesignation.getText().toString().trim();
        String inchargeMobileNumber = etInchargeMobileNumber.getText().toString().trim();
        String companyMobileNumber = etCompanyMobileNumber.getText().toString().trim();
        String branch = etBranch.getText().toString().trim();
        String address = etAddress.getText().toString().trim();
        String city = etCity.getText().toString().trim();
        String state = etState.getText().toString().trim();
        String pinCode = etPincode.getText().toString().trim();
        String gst = etGST.getText().toString().trim();
        String pan = etPAN.getText().toString().trim();

        // individual
        String customerName = etCustomerName.getText().toString().trim();
        String customerMobileNumber = etCustomerMobileNumber.getText().toString().trim();
        String customerAddress = etCustomerAddress.getText().toString().trim();
        String customerCity = etCustomerCity.getText().toString().trim();
        String customerState = etCustomerState.getText().toString().trim();
        String customerPincode = etCustomerPincode.getText().toString().trim();
        String customerPAN = etCustomerPAN.getText().toString().trim();
        if (listener != null)
            listener.onProceedClicked(isCompany, companyName, inchargeName, inchargeDestination, inchargeMobileNumber,
                    companyMobileNumber, branch, address, city, state, pinCode, gst, pan, customerName, customerMobileNumber,
                    customerAddress, customerCity, customerState, customerPincode, customerPAN);
    }

    @OnClick(R.id.ivClose)
    public void onCloseClicked() {
        dismiss();
    }

    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewUnBinder != null) {
            viewUnBinder.unbind();
        }
    }

    public interface Listener {
        void onProceedClicked(boolean isCompany, String companyName, String inchargeName, String inchargeDestination, String inchargeMobileNumber, String companyMobileNumber,
                              String branch, String address, String city, String state, String pinCode, String gst, String pan, String customerName, String customerMobileNumber, String customerAddress,
                              String customerCity, String customerState, String customerPincode, String customerPAN);
    }
}