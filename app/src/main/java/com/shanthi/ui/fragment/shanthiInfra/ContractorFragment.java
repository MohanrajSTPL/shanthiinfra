package com.shanthi.ui.fragment.shanthiInfra;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.shanthi.R;

import com.shanthi.R;
import com.shanthi.model.construction.Expense;
import com.shanthi.model.shanthiInfra.ContractExpense;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.shanthi.utils.ConversionUtils.getJsonFromString;

public class ContractorFragment extends DialogFragment {


    @BindView(R.id.tvWork)
    TextView tvWork;

    @BindView(R.id.tvExpenseName)
    TextView tvExpenseName;

    @BindView(R.id.tvSite)
    TextView tvSite;

    @BindView(R.id.tvAmount)
    TextView tvAmount;

    @BindView(R.id.tvReason)
    TextView tvReason;

    @BindView(R.id.tvDate)
    TextView tvExpDate;

    //    @BindView(R.id.tvDesc)
//    TextView tvDesc;
    private ContractExpense order;
    private List<Expense> orderItemList = new ArrayList<>();
    private Listener listener;
    String quant = "", flg = "1";
    private Unbinder viewUnBinder;



    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public ContractorFragment() {
    }


    public static ContractorFragment newInstance() {

        Bundle args = new Bundle();
        ContractorFragment fragment = new ContractorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.contractor_list_fragment, container, false);

    }
    public void setOrderItemList(List<Expense> orderItemList) {
        this.orderItemList = orderItemList;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewUnBinder = ButterKnife.bind(this, view);
        //String ordeer = getArguments().getString("Expense",orderStr);
        String orderStr = getArguments().getString("Expense");
        order = getJsonFromString(orderStr, ContractExpense.class);

        tvExpenseName.setText(order.getContractorName());
        tvWork.setText(order.getWork());
        tvSite.setText(order.getSite());
        tvReason.setText(order.getReason());
        tvAmount.setText(order.getAmount());
        tvExpDate.setText(order.getPaymentDate());


    }


    @OnClick(R.id.ivClose)
    public void onCloseClicked() {
        dismiss();
    }



    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewUnBinder != null) {
            viewUnBinder.unbind();
        }
    }

    public interface Listener {
    }

}



