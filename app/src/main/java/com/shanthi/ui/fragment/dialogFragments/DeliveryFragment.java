package com.shanthi.ui.fragment.dialogFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.shanthi.R;
import com.shanthi.model.order.OrderDetails;
import com.shanthi.model.order.OrderItem;
import com.shanthi.utils.AppUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class DeliveryFragment extends DialogFragment {


    @BindView(R.id.etDeliveryDate)
    EditText etDeliveryDate;
    @BindView(R.id.etDeliveryTime)
    EditText etDeliveryTime;
    private List<OrderItem> orderItemList = new ArrayList<>();

    private OrderItem driver;
    private Listener listener;
    private String driverName, truckId,truck;
    private int year, month, day;
    private int hour,minute,second ,date;

    private Unbinder viewUnBinder;

    public void setOrderItemList(List<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public DeliveryFragment() {
    }

    public static DeliveryFragment newInstance() {
        Bundle args = new Bundle();
        DeliveryFragment fragment = new DeliveryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.delivery_item_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewUnBinder = ButterKnife.bind(this, view);
        setCancelable(true);

        initDate();
        initTime();
    }
    private void initDate() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        String date = year + "-" + AppUtils.getMonthHrMinStr((month + 1)) + "-" + AppUtils.getMonthHrMinStr(day);
        etDeliveryDate.setText(date);
    }
    private void initTime() {
        String currentTime = new SimpleDateFormat("KK:mm:ss a").format(new Date());
        etDeliveryTime.setText(currentTime);
    }

    @OnClick(R.id.ivClose)
    public void onCloseClicked() {
        dismiss();
    }

    @OnClick(R.id.btnProceed)
    public void onProceedClicked() {
        if (listener != null) {


            OrderDetails delivery = new OrderDetails();

            delivery.setDeliveryDate(etDeliveryDate.getText().toString().trim());
            delivery.setDeliveryTime(etDeliveryTime.getText().toString().trim());

            listener.onProceedClicked(delivery);

        }
    }

    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewUnBinder != null) {
            viewUnBinder.unbind();
        }
    }

    public interface Listener {
        void onProceedClicked(OrderDetails orderDetails);
    }





}