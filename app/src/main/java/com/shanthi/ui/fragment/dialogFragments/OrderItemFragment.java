package com.shanthi.ui.fragment.dialogFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shanthi.R;
import com.shanthi.model.order.OrderDetails;
import com.shanthi.model.order.Uom;
import com.shanthi.model.shanthiInfra.ContractExpense;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class OrderItemFragment extends DialogFragment {


    @BindView(R.id.tvCompanyName)
    TextView tvCompanyName;
    @BindView(R.id.tvTransType)
    TextView tvTransType;
    @BindView(R.id.tvInvoiceDate)
    TextView tvInvoiceDate;
    @BindView(R.id.tvDesc)
    TextView tvDesc;

    private List<ContractExpense> orderItemList = new ArrayList<>();

    private ContractExpense orderItemDetails = null;


    private Listener listener;
    private String driverName, truckId, truckNumber;
    private int year, month, day;
    private int hour, minute, second, date;
    private String uomName, uomId, uomValue;
    private Uom uom;

    private Unbinder viewUnBinder;

    public void setOrderItemList(List<ContractExpense> orderItemList) {
        this.orderItemList = orderItemList;
    }


    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public OrderItemFragment() {
    }

    public static OrderItemFragment newInstance() {
        Bundle args = new Bundle();
        OrderItemFragment fragment = new OrderItemFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.order_item_fragment, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewUnBinder = ButterKnife.bind(this, view);
        setCancelable(true);
      //  String myValue = this.getArguments().getString("message");


     //   if (orderItemDetails != null) {
          //  tvCompanyName.setText(myValue);
          //   tvTransType.setText(orderItemDetails.getContractorName());
        //}
    }

    @OnClick(R.id.ivClose)
    public void onCloseClicked() {
        dismiss();
    }




    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewUnBinder != null) {
            viewUnBinder.unbind();
        }
    }

    public interface Listener {
        void onProceedClicked(OrderDetails orderDetails);
    }


}