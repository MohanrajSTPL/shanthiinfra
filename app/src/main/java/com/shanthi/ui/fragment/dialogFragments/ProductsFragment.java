package com.shanthi.ui.fragment.dialogFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.shanthi.R;
import com.shanthi.model.enquiry.Frequency;
import com.shanthi.model.enquiry.Product;
import com.shanthi.model.enquiry.Products;
import com.shanthi.ui.adapter.enquiry.FrequencySpinnerAdapter;
import com.shanthi.ui.adapter.enquiry.ProductSpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ProductsFragment extends DialogFragment {
    @BindView(R.id.spProduct)
    Spinner spProduct;
    @BindView(R.id.spFrequency)
    Spinner spFrequency;
    @BindView(R.id.etUnit)
    EditText etUnit;
    @BindView(R.id.etMinRequirement)
    EditText etMinRequirement;
    @BindView(R.id.etMaxRequirement)
    EditText etMaxRequirement;

    private List<Product> productList = new ArrayList<>();
    private List<Frequency> frequencyList = new ArrayList<>();
    private String unitId, productName, productId, frequencyId, frequencyName, enquiryStatus;

    private Unbinder viewUnBinder;

    private Listener listener;

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public void setFrequencyList(List<Frequency> frequencyList) {
        this.frequencyList = frequencyList;
    }

    public ProductsFragment() {
    }

    public static ProductsFragment newInstance() {
        Bundle args = new Bundle();
        ProductsFragment fragment = new ProductsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.products_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewUnBinder = ButterKnife.bind(this, view);
        setCancelable(true);
        setProductValues();
        setFrequencyValues();
    }

    @OnClick(R.id.ivClose)
    public void onCloseClicked() {
        dismiss();
    }

    @OnClick(R.id.btnProceed)
    public void onProceedClicked() {
        if (listener != null) {
            if (productId == null) {
                showToast(getString(R.string.select_driver));
                return;
            }
            if (etUnit.getText().toString().trim().isEmpty()) {
                showToast(getString(R.string.enter_unit));
                return;
            }
            if (etMinRequirement.getText().toString().trim().isEmpty()) {
                showToast(getString(R.string.enter_min_requirement));
                return;
            }
            if (etMaxRequirement.getText().toString().trim().isEmpty()) {
                showToast(getString(R.string.enter_max_requirement));
                return;
            }
            if (frequencyId == null) {
                showToast(getString(R.string.select_frequency));
                return;
            }
            Products products = new Products();
            products.setProduct(productName);
            products.setProductId(productId);
            products.setUnitId(unitId);
            products.setUnit(etUnit.getText().toString().trim());
            products.setMinRequirement(etMinRequirement.getText().toString().trim());
            products.setMaxRequirement(etMaxRequirement.getText().toString().trim());
            products.setFrequency(frequencyName);
            products.setFrequencyId(frequencyId);

            listener.onProceedClicked(products);
        }
    }

    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewUnBinder != null) {
            viewUnBinder.unbind();
        }
    }

    public interface Listener {
        void onProceedClicked(Products products);
    }


    private void setProductValues() {
        List<Product> productList1 = new ArrayList<>();
        Product productDummy = new Product();
        productDummy.setProdName("Select Product");
        productList1.add(productDummy);
        productList1.addAll(productList);
        ProductSpinnerAdapter productSpinnerAdapter = new ProductSpinnerAdapter(getContext(),
                R.layout.spinner_list_item, productList1);
        spProduct.setAdapter(productSpinnerAdapter); // Set the custom adapter to the spinner
        // You can create an anonymous listener to handle the event when is selected an spinner item
        spProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                if (position != 0) {
                    Product product = productSpinnerAdapter.getItem(position);
                    if (product != null) {
                        productName = product.getProdName();
                        productId = product.getProdId();
                //        etUnit.setText(product.getUnitDescription());
                  //      unitId = product.getUnit();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });
    }

    private void setFrequencyValues() {

        List<Frequency> frequencyList1 = new ArrayList<>();
        Frequency frequencyDummy = new Frequency();
        frequencyDummy.setFrequencyName("Select Frequency");
        frequencyList1.add(frequencyDummy);
        frequencyList1.addAll(frequencyList);
        FrequencySpinnerAdapter frequencySpinnerAdapter = new FrequencySpinnerAdapter(getContext(),
                R.layout.spinner_list_item,
                frequencyList1);
        spFrequency.setAdapter(frequencySpinnerAdapter); // Set the custom adapter to the spinner
        // You can create an anonymous listener to handle the event when is selected an spinner item
        spFrequency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                if (position != 0) {
                    Frequency frequency = frequencySpinnerAdapter.getItem(position);
                    if (frequency != null) {
                        frequencyName = frequency.getFrequencyName();
                        frequencyId = frequency.getFrequencyId();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });
    }
}