package com.shanthi.ui.fragment.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shanthi.R;
//import com.bass.ui.activity.enquiry.EnquiryListActivity;

import com.shanthi.ui.activity.reports.ChartActivity;
import com.shanthi.ui.fragment.BaseFragment;


public class HomeFragment extends BaseFragment {

//    @BindView(R.id.barChart)
//    BarChart barChart;


    public HomeFragment() {
    }
    private String Status = "null";

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showOrderListPage();
    }

//    @OnClick(R.id.llListView)
//    public void onFollowUpClick() {
//        Status="Done";
//        showOrderListPage();
//    }

//    @OnClick(R.id.llputhiya_ordergal)
//    public void OnNew_pending_order() {
//        Status="Pending";
//        showOrderListPage();
//    }

    private void showOrderListPage() {
        Intent intent = new Intent(getActivity(), ChartActivity.class);
        startActivity(intent);
    }

}