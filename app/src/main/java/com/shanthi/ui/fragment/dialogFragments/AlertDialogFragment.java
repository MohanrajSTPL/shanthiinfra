package com.shanthi.ui.fragment.dialogFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shanthi.R;

import butterknife.BindView;
import butterknife.OnClick;

public class AlertDialogFragment extends BaseDialogFragment {
    private final String TAG = "AlertDialogFragment";

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvMessage)
    TextView tvMessage;
    @BindView(R.id.tvYes)
    TextView tvYes;
    @BindView(R.id.tvNo)
    TextView tvNo;

    private static final String TITLE = "title";
    private static final String MESSAGE = "message";
    private static final String POSITIVE_TEXT = "positive_text";
    private static final String NEGATIVE_TEXT = "negative_text";
    private AlertDialogFragment.AlertDialogListener listener;

    public void setListener(AlertDialogFragment.AlertDialogListener listener) {
        this.listener = listener;
    }

    public AlertDialogFragment() {
    }

    public static AlertDialogFragment newInstance(String title, String message) {
        AlertDialogFragment frag = new AlertDialogFragment();
        Bundle args = new Bundle();
        args.putString(TITLE, title);
        args.putString(MESSAGE, message);
        frag.setArguments(args);
        return frag;
    }

    public static AlertDialogFragment newInstance(String title, String message, String positiveText, String negativeText) {
        AlertDialogFragment frag = new AlertDialogFragment();
        Bundle args = new Bundle();
        args.putString(TITLE, title);
        args.putString(MESSAGE, message);
        args.putString(POSITIVE_TEXT, positiveText);
        args.putString(NEGATIVE_TEXT, negativeText);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_alert_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setCancelable(false);

        Bundle args = getArguments();
        if (args != null) {
            String title = args.getString(TITLE);
            if (title != null) {
                if (title.isEmpty())
                    tvTitle.setVisibility(View.GONE);
                tvTitle.setText(title);
            }

            String message = args.getString(MESSAGE);
            if (message != null) {
                if (message.isEmpty())
                    tvMessage.setVisibility(View.GONE);
                tvMessage.setText(message);
            }

            String positiveText = args.getString(POSITIVE_TEXT);
            if (positiveText != null)
                tvYes.setText(positiveText);

            String negativeText = args.getString(NEGATIVE_TEXT);
            if (negativeText != null) {
                if (negativeText.isEmpty())
                    tvNo.setVisibility(View.GONE);
                tvNo.setText(negativeText);
            }
        }
    }

    @OnClick(R.id.tvYes)
    public void onYesClicked() {
        if (listener != null) {
            listener.onYesClicked();
        }
    }

    @OnClick(R.id.tvNo)
    public void onNoClicked() {
        if (listener != null) {
            listener.onNoClicked();
        }
    }

    public interface AlertDialogListener {
        void onYesClicked();

        void onNoClicked();
    }
}
