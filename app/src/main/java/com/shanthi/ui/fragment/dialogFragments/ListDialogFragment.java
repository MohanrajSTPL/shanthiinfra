package com.shanthi.ui.fragment.dialogFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shanthi.R;
import com.shanthi.ui.adapter.listDialog.ListDialogAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.shanthi.utils.LogUtils.makeLogTag;

public class ListDialogFragment<T> extends BaseDialogFragment implements
        ListDialogAdapter.ListDialogItemClickListener<T> {
    private static final String TAG = makeLogTag("ListDialogFragment");

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.llSearch)
    LinearLayout llSearch;
    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.rvItems)
    RecyclerView rvItems;

    private String title;
    private List<T> items = new ArrayList<>();
    private ListDialogAdapter<T> adapter;
    private ListDialogItemSelectionListener<T> listener;

    public ListDialogFragment() {
        // Required empty public constructor
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public void setListener(ListDialogItemSelectionListener<T> listener) {
        this.listener = listener;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setCancelable(true);

        /*if (isSearch) {
            etSearch.setHint(getString(R.string.search));
            llSearch.setVisibility(View.VISIBLE);
        } else {
            llSearch.setVisibility(View.GONE);
        }*/

        if (items.size() > 10) {
            etSearch.setHint(getString(R.string.search));
            llSearch.setVisibility(View.VISIBLE);
        } else {
            llSearch.setVisibility(View.GONE);
        }

        rvItems.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvItems.setHasFixedSize(true);
        adapter = new ListDialogAdapter<>();
        adapter.setListener(this);
        adapter.setItems(items);
        tvTitle.setText(title);
        rvItems.setAdapter(adapter);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filter(editable.toString());
            }
        });
    }

    private void filter(String text) {
        List<T> filteredList = new ArrayList<>();

        // Looping through existing elements
        for (T t : items) {
            // If the existing elements contains the search input
            if (t.toString().toLowerCase().contains(text.toLowerCase())) {
                // Adding the element to filtered list
                filteredList.add(t);
            }
        }

        //calling a method of the adapter class and passing the filtered list
        adapter.filterList(filteredList);
    }

    @Override
    public void onListDialogItemClicked(T item) {
        hideKeyBoard(getView());
        if (listener != null)
            listener.onListDialogItemSelected(item);
        dismiss();
    }

    @OnClick(R.id.tvCancel)
    public void onCancelClicked() {
        dismiss();
    }

    public interface ListDialogItemSelectionListener<T> {
        void onListDialogItemSelected(T item);
    }
}
