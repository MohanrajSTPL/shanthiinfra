package com.shanthi.ui.customView;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.shanthi.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashItemView extends CardView {
    @BindView(R.id.ivIcon)
    ImageView ivIcon;
    @BindView(R.id.tvText)
    TextView tvText;

    public DashItemView(@NonNull Context context) {
        super(context);
        init(context, null);
    }

    public DashItemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public DashItemView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        View rootView = LayoutInflater.from(context).inflate(R.layout.dash_item_view, this, true);
        // Use butterknife to bind views
        ButterKnife.bind(rootView);

        TypedArray typedArray;
        int textRes;
        int iconRes;

        if (attrs != null) {
            typedArray = context.obtainStyledAttributes(attrs, R.styleable.DashItemView);
            try {
                textRes = typedArray.getResourceId(R.styleable.DashItemView_textRes, R.string.empty_text);
                iconRes = typedArray.getResourceId(R.styleable.DashItemView_iconRes, R.drawable.empty_drawable);
            } finally {
                typedArray.recycle();
            }

            ivIcon.setImageResource(iconRes);
            tvText.setText(textRes);
        }
    }
}
