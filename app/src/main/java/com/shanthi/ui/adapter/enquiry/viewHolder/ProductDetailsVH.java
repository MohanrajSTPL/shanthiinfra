package com.shanthi.ui.adapter.enquiry.viewHolder;

import android.view.View;
import android.widget.TextView;

import com.shanthi.R;
import com.shanthi.model.enquiry.Products;
import com.shanthi.ui.adapter.enquiry.ProductDetailsAdapter;
import com.shanthi.ui.base.BaseVH;

import butterknife.BindView;
import butterknife.OnClick;

public class ProductDetailsVH extends BaseVH {

    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvMobileNumber)
    TextView tvMobileNumber;
    @BindView(R.id.tvProduct)
    TextView tvProduct;
    @BindView(R.id.tvUnit)
    TextView tvUnit;
    @BindView(R.id.tvMinRequirement)
    TextView tvMinRequirement;
    @BindView(R.id.tvMaxRequirement)
    TextView tvMaxRequirement;
    @BindView(R.id.tvFrequency)
    TextView tvFrequency;
    @BindView(R.id.tvEnquiryStatus)
    TextView tvEnquiryStatus;
    @BindView(R.id.tvRemove)
    TextView tvRemove;
/*
    @BindView(R.id.tvCustomerType)
    TextView tvCustomerType;
*/

    private Products products;
    private ProductDetailsAdapter.ProductClickListener listener;
    private boolean isRemove = true;

    public ProductDetailsVH(View itemView) {
        super(itemView);
    }

    public void setValues(Products products, ProductDetailsAdapter.ProductClickListener listener, boolean isRemove) {
        this.products = products;
        this.listener = listener;
        this.isRemove = isRemove;

        tvName.setVisibility(View.GONE);
        tvMobileNumber.setVisibility(View.GONE);
        tvEnquiryStatus.setVisibility(View.GONE);

        tvProduct.setText("Product : " + products.getProduct());
        tvUnit.setText("Unit : " + products.getUnit());
        tvMinRequirement.setText("Min Requirement : " + products.getMinRequirement() + " " + products.getUnit());
        tvMaxRequirement.setText("Max Requirement : " + products.getMaxRequirement() + " " + products.getUnit());
        tvFrequency.setText("Frequency : " + products.getFrequency());
        tvEnquiryStatus.setText("EnquiryStatus : " + products.getEnquiryStatus());
        if (!isRemove) {
            tvRemove.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.tvRemove)
    public void onRemove() {
        if (listener != null)
            listener.onRemoveProduct(products, getAdapterPosition());
    }
}