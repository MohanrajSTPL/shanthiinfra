package com.shanthi.ui.adapter.decline;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shanthi.R;
import com.shanthi.model.construction.Expense;
import com.shanthi.ui.adapter.decline.viewHolder.DeclineListVH;
import com.shanthi.ui.adapter.expense.ExpenseListAdapter;
import com.shanthi.ui.adapter.expense.viewHolder.ExpenseListVH;

import java.util.ArrayList;
import java.util.List;

public class DeclineListAdapter extends RecyclerView.Adapter<DeclineListVH> {
    private DeclineListAdapter.OrderListener listener;
    private List<Expense> orderList = new ArrayList<>();

    public void setListener(DeclineListAdapter.OrderListener listener) {
        this.listener = listener;
    }

    public void setOrderList(List<Expense> orderList) {
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public DeclineListVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.activity_desc_list, viewGroup, false);
        return new DeclineListVH(item);
    }

    @Override
    public void onBindViewHolder(@NonNull DeclineListVH orderListVH, int position) {
        orderListVH.setValues(orderList.get(position), listener);
    }

    public void filterList(List<Expense> filteredList) {
        this.orderList = filteredList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return orderList == null ? 0 : orderList.size();
    }

    public interface OrderListener {
        void onOrderClicked(Expense order);

        void onOrderRemoveClicked(Expense order);

        void onDelete(Expense order);
    }
}
