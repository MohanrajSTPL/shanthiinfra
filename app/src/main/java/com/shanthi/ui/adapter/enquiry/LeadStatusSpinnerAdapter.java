package com.shanthi.ui.adapter.enquiry;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.shanthi.model.enquiry.LeadStatus;

import java.util.List;

public class LeadStatusSpinnerAdapter extends ArrayAdapter<LeadStatus> {

    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private List<LeadStatus> leadStatusList;

    public LeadStatusSpinnerAdapter(Context context, int textViewResourceId,
                                    List<LeadStatus> leadStatusList) {
        super(context, textViewResourceId, leadStatusList);
        this.context = context;
        this.leadStatusList = leadStatusList;
    }

    @Override
    public int getCount() {
        return leadStatusList.size();
    }

    @Override
    public LeadStatus getItem(int position) {
        return leadStatusList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(leadStatusList.get(position).getLeadStatusName());

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,
                                @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(leadStatusList.get(position).getLeadStatusName());

        return label;
    }
}