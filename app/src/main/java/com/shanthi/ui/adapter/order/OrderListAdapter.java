package com.shanthi.ui.adapter.order;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shanthi.R;
import com.shanthi.model.construction.Approval;
import com.shanthi.ui.adapter.order.viewHolder.OrderListVH;

import java.util.ArrayList;
import java.util.List;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListVH> {

    private OrderListener listener;
    private List<Approval> orderList = new ArrayList<>();

    public void setListener(OrderListener listener) {
        this.listener = listener;
    }

    public void setOrderList(List<Approval> orderList) {
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public OrderListVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.order_list_item, viewGroup, false);
        return new OrderListVH(item);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderListVH orderListVH, int position) {
        orderListVH.setValues(orderList.get(position), listener);
    }

    public void filterList(List<Approval> filteredList) {
        this.orderList = filteredList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return orderList == null ? 0 : orderList.size();
    }

    public interface OrderListener {
        void onOrderClicked(Approval order);
        void onOrderRemoveClicked(Approval order);

        void onDeclineExpense(Approval order);
        // void onDeclineExpense(Approval order);
    }


}
