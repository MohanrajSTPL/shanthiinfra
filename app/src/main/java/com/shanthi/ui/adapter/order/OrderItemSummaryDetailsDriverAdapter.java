package com.shanthi.ui.adapter.order;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shanthi.R;
import com.shanthi.model.List.NewList;
import com.shanthi.ui.adapter.order.viewHolder.OrderItemSummaryDetailsDriverVH;

import java.util.ArrayList;
import java.util.List;

public class OrderItemSummaryDetailsDriverAdapter extends RecyclerView.Adapter<OrderItemSummaryDetailsDriverVH> {

    public static Object OrderItemDriverDetailListener;
    private OrderItemDriverDetailListener listener;
    private List<NewList> orderItemDetailsList = new ArrayList<>();
    private String StatusFlag;

    public void setListener(OrderItemDriverDetailListener listener) {
        this.listener = listener;
    }

         // public void setOrderItemDetailsList(List<NewList> orderItemDetailsList, String StatusFlag) {
        // this.orderItemDetailsList = orderItemDetailsList;
       // this.StatusFlag = StatusFlag;
      //    }

    @NonNull
    @Override
    public OrderItemSummaryDetailsDriverVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.order_driver_delivery_list, viewGroup, false);
        return new OrderItemSummaryDetailsDriverVH(item);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderItemSummaryDetailsDriverVH OrderItemSummaryDetailsDriverVH, int position) {
        OrderItemSummaryDetailsDriverVH.setValues(orderItemDetailsList.get(position), listener,StatusFlag);
    }

    public void filterList(List<NewList> filteredList) {
        this.orderItemDetailsList = filteredList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return orderItemDetailsList == null ? 0 : orderItemDetailsList.size();
    }

    public void setOrderItemDetailsList(List<NewList> orderList) {

        this.orderItemDetailsList = orderList;
    }

    public interface OrderItemDriverDetailListener {
        void onOrderItemDriverDetailClicked(NewList orderItemDetails);

    }

}
