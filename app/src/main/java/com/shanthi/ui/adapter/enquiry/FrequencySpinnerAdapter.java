package com.shanthi.ui.adapter.enquiry;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.shanthi.model.enquiry.Frequency;

import java.util.List;

public class FrequencySpinnerAdapter extends ArrayAdapter<Frequency> {

    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private List<Frequency> frequencyList;

    public FrequencySpinnerAdapter(Context context, int textViewResourceId,
                                   List<Frequency> frequencyList) {
        super(context, textViewResourceId, frequencyList);
        this.context = context;
        this.frequencyList = frequencyList;
    }

    @Override
    public int getCount() {
        return frequencyList.size();
    }

    @Override
    public Frequency getItem(int position) {
        return frequencyList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(frequencyList.get(position).getFrequencyName());

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,
                                @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(frequencyList.get(position).getFrequencyName());

        return label;
    }
}