package com.shanthi.ui.adapter.contractor;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shanthi.R;

import com.shanthi.model.shanthiInfra.ContractExpense;
import com.shanthi.ui.adapter.contractor.viewHolder.ContractorListVH;

import java.util.ArrayList;
import java.util.List;

public class ContractorListAdapter extends RecyclerView.Adapter<ContractorListVH>{
    private ContractorListAdapter.OrderListener listener;
    private List<ContractExpense> orderList = new ArrayList<>();

    public void setListener(ContractorListAdapter.OrderListener listener) {
        this.listener = listener;
    }

    public void setOrderList(List<ContractExpense> orderList) {
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public ContractorListVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.activity_contract_list, viewGroup, false);
        return new ContractorListVH(item);
    }

    @Override
    public void onBindViewHolder(@NonNull ContractorListVH orderListVH, int position) {
        orderListVH.setValues(orderList.get(position), listener);
    }

    public void filterList(List<ContractExpense> filteredList) {
        this.orderList = filteredList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return orderList == null ? 0 : orderList.size();
    }

    public interface OrderListener {
        void onOrderClicked(ContractExpense order);
        void onOrderRemoveClicked(ContractExpense order);
    }
}
