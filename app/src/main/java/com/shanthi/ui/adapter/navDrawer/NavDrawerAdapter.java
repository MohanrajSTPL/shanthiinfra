package com.shanthi.ui.adapter.navDrawer;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shanthi.R;
import com.shanthi.model.navDrawer.NavDrawerItem;
import com.shanthi.ui.adapter.navDrawer.viewHolders.HeaderVH;
import com.shanthi.ui.adapter.navDrawer.viewHolders.ItemVH;

import java.util.List;

public class NavDrawerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int HEADER = 0;
    private static final int ITEM = 1;

    private List<NavDrawerItem> items;
    private NavDrawerItemClickListener listener;
    private int selectedPosition = 2;

    public void setItems(List<NavDrawerItem> items) {
        this.items = items;
    }

    public void setListener(NavDrawerItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        switch (viewType) {
            case HEADER:
                View header = inflater.inflate(R.layout.nav_drawer_header, parent, false);
                return new HeaderVH(header);
            case ITEM:
                View item = inflater.inflate(R.layout.nav_drawer_item, parent, false);
                return new ItemVH(item);
            default:
                throw new RuntimeException("onCreateViewHolder:Wrong view type");
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int viewType = holder.getItemViewType();

        switch (viewType) {
            case HEADER:
                HeaderVH headerVH = (HeaderVH) holder;
                headerVH.setHeader(listener);
                break;
            case ITEM:
                NavDrawerItem item = items.get(position - 1);
                ItemVH itemVH = (ItemVH) holder;
                itemVH.setItem(item, this, listener);
                changeBackgroundColor(itemVH, position);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() + 1 : 1;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return HEADER;
            default:
                return ITEM;
        }
    }

    public void onItemClicked(int position) {
        selectedPosition = position;
        notifyDataSetChanged();
    }

    public void setDefaultPosition() {
        selectedPosition = 2;
        notifyDataSetChanged();
    }

    private void changeBackgroundColor(ItemVH itemVH, int position) {
        if (selectedPosition == position) {
            itemVH.changeBackgroundColor(true);
        } else {
            itemVH.changeBackgroundColor(false);
        }
    }

    public interface NavDrawerItemClickListener {
        void onNavDrawerItemClicked(String title, int position);
    }
}
