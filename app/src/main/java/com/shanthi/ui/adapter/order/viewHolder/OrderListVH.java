package com.shanthi.ui.adapter.order.viewHolder;

import android.view.View;
import android.widget.TextView;

import com.shanthi.R;
import com.shanthi.model.construction.Approval;
import com.shanthi.ui.adapter.order.OrderListAdapter;
import com.shanthi.ui.base.BaseVH;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Optional;

public class OrderListVH extends BaseVH {

    @BindView(R.id.tvInvoiceId)
    TextView tvInvoiceId;
    @BindView(R.id.tvProductName)
    TextView tvProductName;
    @BindView(R.id.tvInvoiceDate)
    TextView tvInvoiceDate;
    @BindView(R.id.tvQty)
    TextView tvQty;
    @BindView(R.id.tvCompanyName)
    TextView tvCompanyName;
    @BindView(R.id.tvTransType)
    TextView tvTransType;
    @BindView(R.id.tvId)
    TextView tvId;
    @BindView(R.id.tvVehNo)
    TextView tvVehNo;
    @BindView(R.id.tvEwayNo)
    TextView tvEwayNo;
    @BindView(R.id.tvNoOfItems)
    TextView tvNoOfItems;
    @BindView(R.id.remove)
    TextView remove;
    @BindView(R.id.tvDesc)
    TextView tvDesc;
    @BindView(R.id.ListDescription)
    TextView ListDescription;

    String status  ="Done";
    String status1 = "Processing Order";
    private Approval order;
    private OrderListAdapter.OrderListener listener;
    String pay1 = "Credit";
    String pay2 = "Cash";

    public OrderListVH(View itemView) {
        super(itemView);
    }

    public void setValues(Approval order, OrderListAdapter.OrderListener listener) {
        this.order = order;
        this.listener = listener;

       // Integer orderValue = Integer.parseInt(order.getInvoiceId());

        // Integer total= (orderValue + 1000);
        tvInvoiceId.setText("Expense Id : " + order.getExpenseID());
        tvTransType.setText("Expense Date :" + order.getExpenseDate());
        tvCompanyName.setText("Site : " + order.getSite());
        tvVehNo.setText("Amount:" + order.getAmount());
       // tvDesc.setText("Description : " + order.getDescription());
        tvInvoiceDate.setText("Customer : " + order.getCustomer());
        if(order.getDescription()!= null && !order.getDescription().isEmpty()) {
            tvDesc.setText("Description : " + order.getDescription());
        }
        else
        {
            tvDesc.setText("Description : " + "Not Specified");
        }
      //  tvDesc.setText("Description : " + order.getDescription());
//        if(order.getEwaynumber()!= null && !order.getEwaynumber().isEmpty()) {
//            tvEwayNo.setText("Eway Number: " + order.getEwaynumber());
//        }
//        else {
//            tvEwayNo.setText("Eway Number: " + "Not Specified");
//        }
//        tvQty.setText("Quantity: " + order.getQuantity());
//        tvId.setText("Customer Id :" + order.getId());
//        tvProductName.setText("Product Name :" + order.getProductName());
//        tvNoOfItems.setText("No Of Items: " + order.getNoOfItems());
//        if(order.getVehiclenumber()!= null && !order.getVehiclenumber().isEmpty()) {
//            tvVehNo.setText("Vehicle Number: " + order.getVehiclenumber());
//        }
//        else {
//            tvVehNo.setText("Vehicle Number: " + "Not Specified");
//        }


    }

    @OnClick(R.id.llItem)
    public void onOrderUpClicked() {
        if (listener != null)
            listener.onOrderClicked(order);
    }

    @Optional
    @OnClick(R.id.remove)
    public void onOrderRemoveClicked() {
        if (listener != null)
            listener.onOrderRemoveClicked(order);
    }

//    @Optional
//    @OnClick(R.id.decline)
//    public void onOrderClicked() {
//        if (listener != null)
//            listener.onOrderRemoveClicked(order);
//    }
    @Optional
    @OnClick(R.id.decline)
    public void onDeclineExpense() {
        if (listener != null)
            listener.onDeclineExpense(order);
    }
}