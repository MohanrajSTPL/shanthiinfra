package com.shanthi.ui.adapter.expense.viewHolder;

import android.view.View;
import android.widget.TextView;

import com.shanthi.R;
import com.shanthi.model.construction.Expense;
import com.shanthi.ui.adapter.expense.ExpenseListAdapter;
import com.shanthi.ui.base.BaseVH;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Optional;

public class ExpenseListVH extends BaseVH {

    @BindView(R.id.tvExpType)
    TextView tvExpType;

    @BindView(R.id.tvAmount)
    TextView tvAmount;

    @BindView(R.id.tvDesc)
    TextView tvDesc;

    @BindView(R.id.tvExpDate)
    TextView tvExpDate;

    String status  ="Done";
    String status1 = "Processing Order";
    private Expense order;
    private ExpenseListAdapter.OrderListener listener;
    String pay1 = "Credit";
    String pay2 = "Cash";

    public ExpenseListVH(View itemView) {
        super(itemView);
    }

    public void setValues(Expense order, ExpenseListAdapter.OrderListener listener) {
        this.order = order;
        this.listener = listener;

        // Integer orderValue = Integer.parseInt(order.getInvoiceId());

        // Integer total= (orderValue + 1000);
        // tvInvoiceId.setText("Expense Id : " + order.getExpenseId());
        tvExpDate.setText( order.getExpenseDate());
        // tvCompanyName.setText("Customer : " + order.getCustomer());
        // tvInvoiceDate    .setText("Site : " + order.getSite());
        tvAmount.setText(order.getAmount());
        // tvEwayNo.setText("Reason :" + order.getReason());
       // tvExpType.setText( order.getExpenseName());
       tvExpType.setText( order.getExpenseName());
        if(order.getReason()!=null && !order.getReason().isEmpty()) {
            tvDesc.setText(order.getReason());
        }
        else {
            tvDesc.setText("-");
        }
//        if(order.getEwaynumber()!= null && !order.getEwaynumber().isEmpty()) {
//            tvEwayNo.setText("Eway Number: " + order.getEwaynumber());
//        }
//        else {
//            tvEwayNo.setText("Eway Number: " + "Not Specified");
//        }
//        tvQty.setText("Quantity: " + order.getQuantity());
//        tvId.setText("Customer Id :" + order.getId());
//        tvProductName.setText("Product Name :" + order.getProductName());
//        tvNoOfItems.setText("No Of Items: " + order.getNoOfItems());
//        if(order.getVehiclenumber()!= null && !order.getVehiclenumber().isEmpty()) {
//            tvVehNo.setText("Vehicle Number: " + order.getVehiclenumber());
//        }
//        else {
//            tvVehNo.setText("Vehicle Number: " + "Not Specified");
//        }


    }

    @OnClick(R.id.llItem)
    public void onOrderUpClicked() {
        if (listener != null)
            listener.onOrderClicked(order);
    }

    @Optional
    @OnClick(R.id.remove)
    public void onOrderRemoveClicked() {
        if (listener != null)
            listener.onOrderRemoveClicked(order);
    }

}
