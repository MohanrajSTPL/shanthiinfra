package com.shanthi.ui.adapter.PL.viewHolder;

import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.shanthi.R;
import com.shanthi.model.shanthiInfra.PL;
import com.shanthi.ui.adapter.PL.PLAdapter;

import com.shanthi.ui.base.BaseVH;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Optional;

public class PLVH extends BaseVH {
    @BindView(R.id.tvCompanyName)
    TextView tvCompanyName;
    @BindView(R.id.tvTransType)
    TextView tvTransType;
    @BindView(R.id.tvInvoiceDate)
    TextView tvInvoiceDate;
    @BindView(R.id.tvVehNo)
    TextView tvVehNo;
    @BindView(R.id.tvEwayNo)
    TextView tvEwayNo;
    @BindView(R.id.tvNetAmount)
    TextView tvNetAmount;
    @BindView(R.id.ListDescription)
    TextView ListDescription;


    String status  ="Done";
    String status1 = "Processing Order";
    private PL order;
    private PLAdapter.OrderListener listener;
    String pay1 = "Credit";
    String pay2 = "Cash";
String site = "0.00";
    public PLVH(View itemView) {
        super(itemView);
    }

    public void setValues(PL order, PLAdapter.OrderListener listener) {
        this.order = order;
        this.listener = listener;
//        if(order.getSiteValue().equals(site))
//        {
//            tvNetAmount.setText("Net Amount :" + "-" + order.getGrand());
//        }
//        else {
//            Integer a = Integer.parseInt(order.getSiteValue());
//            Integer b = Integer.parseInt(order.getGrand());
//            int c = a - b;
//            tvNetAmount.setText("Net Amount :" + c);
//        }
//        String siteValue = order.getSiteValue();
//        String grandTotal =order.getGrand();
//       int net = Integer.parseInt(siteValue) - Integer.parseInt(grandTotal);

          tvCompanyName.setText("SiteName :" +order.getSiteName());
        // tvCompanyName.setText("Customer : " + order.getCustomer());
        // tvInvoiceDate    .setText("Site : " + order.getSite());
         tvTransType.setText("Contractor :" + order.getContractor());
         tvVehNo.setText("SiteExpense :" + order.getSiteExpense());
         tvEwayNo.setText("GrandTotal :" +  order.getGrand());
         tvInvoiceDate.setText("SiteValue :" + order.getSiteValue());
         tvNetAmount.setText("Net Amount :" + " "+ order.getNetTotal());
         tvNetAmount.setTextColor(Color.parseColor("#F8BB0606"));
         ListDescription.setText("Contractor Payment :" + order.getCpayment());
    }

    @OnClick(R.id.llItem)
    public void onOrderUpClicked() {
        if (listener != null)
            listener.onOrderClicked(order);
    }

    @Optional
    @OnClick(R.id.remove)
    public void onOrderRemoveClicked() {
        if (listener != null)
            listener.onOrderRemoveClicked(order);
    }

}
