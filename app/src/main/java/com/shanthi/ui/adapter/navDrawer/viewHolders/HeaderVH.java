package com.shanthi.ui.adapter.navDrawer.viewHolders;

import android.view.View;

import com.shanthi.R;
import com.shanthi.ui.adapter.navDrawer.NavDrawerAdapter;
import com.shanthi.ui.base.BaseVH;

import butterknife.OnClick;

public class HeaderVH extends BaseVH {

    private NavDrawerAdapter.NavDrawerItemClickListener listener;

    public HeaderVH(View itemView) {
        super(itemView);
    }

    public void setHeader(NavDrawerAdapter.NavDrawerItemClickListener listener) {
        this.listener = listener;
    }

    @OnClick(R.id.llHeader)
    public void onHeaderClicked() {
        if (listener != null) {
            listener.onNavDrawerItemClicked("Header", getAdapterPosition());
        }
    }
}
