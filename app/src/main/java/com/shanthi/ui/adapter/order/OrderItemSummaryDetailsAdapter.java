package com.shanthi.ui.adapter.order;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shanthi.R;
import com.shanthi.model.order.ItemList;
import com.shanthi.ui.adapter.order.viewHolder.OrderItemSummaryDetailsVH;

import java.util.ArrayList;
import java.util.List;

public class OrderItemSummaryDetailsAdapter extends RecyclerView.Adapter<OrderItemSummaryDetailsVH> {

    private OrderItemDetailListener listener;
    private List<ItemList> orderItemDetailsList = new ArrayList<>();
    private String StatusFlag;

    public void setListener(OrderItemDetailListener listener) {
        this.listener = listener;
    }

    public void setOrderItemDetailsList(List<ItemList> orderItemDetailsList,String StatusFlag) {
        this.orderItemDetailsList = orderItemDetailsList;
        this.StatusFlag = StatusFlag;
    }

    @NonNull
    @Override
    public OrderItemSummaryDetailsVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.order_item_table_list_item, viewGroup, false);
        return new OrderItemSummaryDetailsVH(item);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderItemSummaryDetailsVH orderItemSummaryDetailsVH, int position) {
        orderItemSummaryDetailsVH.setValues(orderItemDetailsList.get(position), listener,StatusFlag);
    }

    public void filterList(List<ItemList> filteredList) {
        this.orderItemDetailsList = filteredList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return orderItemDetailsList == null ? 0 : orderItemDetailsList.size();
    }

    public void setOrderItemDetailsList(List<ItemList> orderItemDetailsList) {

        this.orderItemDetailsList = orderItemDetailsList;

    }

    public interface OrderItemDetailListener {
        void onOrderItemDetailClicked(ItemList orderItemDetails);
    }

}
