package com.shanthi.ui.adapter.order;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shanthi.R;
import com.shanthi.model.construction.Expense;
import com.shanthi.ui.adapter.order.viewHolder.UserExpListVH;

import java.util.ArrayList;
import java.util.List;

public class UserExpListAdapter extends RecyclerView.Adapter<UserExpListVH> {

    private OrderListener listener;
    private List<Expense> orderList = new ArrayList<>();

    public void setListener(OrderListener listener) {
        this.listener = listener;
    }

    public void setOrderList(List<Expense> orderList) {
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public UserExpListVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.activity_user_exp_list, viewGroup, false);
        return new UserExpListVH(item);
    }

    @Override
    public void onBindViewHolder(@NonNull UserExpListVH orderListVH, int position) {
        orderListVH.setValues(orderList.get(position), listener);
    }

    public void filterList(List<Expense> filteredList) {
        this.orderList = filteredList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return orderList == null ? 0 : orderList.size();
    }

    public interface OrderListener {
        void onOrderClicked(Expense order);
        void onOrderRemoveClicked(Expense order);
    }
}
