package com.shanthi.ui.activity.order;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shanthi.R;
import com.shanthi.model.construction.Expense;
import com.shanthi.model.order.OrderDetails;
import com.shanthi.network.response.expenseType.UserExpenseResponse;
import com.shanthi.network.response.onBoarding.LoginResponse;
import com.shanthi.presenter.order.UserExpListPresenter;
import com.shanthi.ui.adapter.order.UserExpListAdapter;
import com.shanthi.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import nucleus5.factory.RequiresPresenter;

import static com.shanthi.constant.Constants.ACCOUNT_PREFS;
import static com.shanthi.utils.AccountUtils.getLogin;

@RequiresPresenter(UserExpListPresenter.class)
public class OrderListActivity extends BaseActivity<UserExpListPresenter> implements UserExpListAdapter.OrderListener {

    @BindView(R.id.rvOrder)
    RecyclerView rvOrder;
    @BindView(R.id.tvNoData)
    TextView tvNoData;
    @BindView(R.id.llLayout)
    LinearLayout llLayout;
    private UserExpListAdapter adapter;
    private int year, month, day;
    String status = "Done";
    String noOfItems,deliveredGoods;
    private String orderDate,ListDate;
    private String viewstate = "PENDING";
    String date;

    private List<Expense> orderList = new ArrayList<>();
    String state ;
    String expenseId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        setToolbarTitle("Expenses Type");
        setRecyclerView();
        String sessionId = null;
        String userType = null;
        if (getIntent() != null && getIntent().getExtras() != null) {
             expenseId = getIntent().getExtras().getString("expenseId");
        }
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
            sessionId = loginResponse.getLoginDetails().get(0).getSessionid();
            userType = loginResponse.getLoginDetails().get(0).getUserttype();
        }

        Expense expense = new Expense();
        expense.setSessionid(sessionId);
        expense.setExpenseid(expenseId);
        expense.setUsertype(userType);
        getPresenter().getExpenseList(expense);

    }

    private void setRecyclerView() {
        adapter = new UserExpListAdapter();
        adapter.setListener(this);
        rvOrder.setHasFixedSize(false);
        rvOrder.setLayoutManager(new LinearLayoutManager(this));
        rvOrder.setAdapter(adapter);
    }

    public void onUserExpenseListResponseSuccess(UserExpenseResponse response) {
        hideProgress();

       if( response.getMessage().equals("No Expense found")){
           tvNoData.setVisibility(View.VISIBLE);
           llLayout.setVisibility(View.GONE);
       }
        //
        rvOrder.setVisibility(View.VISIBLE);
        if (response.getExpense() != null)
            orderList = response.getExpense();
        adapter.setOrderList(response.getExpense());
        adapter.notifyDataSetChanged();
    }
    @Override
    public void onOrderClicked(Expense order) {

    }

    @Override
    public void onOrderRemoveClicked(Expense order) {

    }

    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) { }

}
