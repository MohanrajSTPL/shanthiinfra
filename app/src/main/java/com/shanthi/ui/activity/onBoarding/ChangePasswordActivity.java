package com.shanthi.ui.activity.onBoarding;

import android.os.Bundle;
import android.widget.EditText;

import com.shanthi.R;
import com.shanthi.model.order.OrderDetails;
import com.shanthi.network.response.HeaderResponse;
import com.shanthi.network.response.onBoarding.LoginResponse;
import com.shanthi.presenter.onBoarding.ChangePasswordPresenter;
import com.shanthi.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;
import nucleus5.factory.RequiresPresenter;

import static com.shanthi.constant.Constants.ACCOUNT_PREFS;
import static com.shanthi.utils.AccountUtils.getLogin;

@RequiresPresenter(ChangePasswordPresenter.class)
public class ChangePasswordActivity extends BaseActivity<ChangePasswordPresenter> {
    @BindView(R.id.etOldPassword)
    EditText etOldPassword;
    @BindView(R.id.etNewPassword)
    EditText etNewPassword;
    @BindView(R.id.etConfirmPassword)
    EditText etConfirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        setToolbarTitle(R.string.change_password);

    }

    @OnClick(R.id.btnSubmit)
    public void onSubmitClicked() {

        if (etOldPassword.getText().toString().isEmpty()) {
            showToast("பழைய கடவுச்சொல் காலியாக இருக்க கூடாது");
        } else if (etNewPassword.getText().toString().isEmpty()) {
            showToast("புதிய கடவுச்சொல் காலியாக இருக்க கூடாது");
        } else if (etOldPassword.getText().toString().equals(etNewPassword.getText().toString())) {
            showToast("பழைய கடவுச்சொல் மற்றும் புதிய கடவுச்சொல் ஒன்று");
        } else if (!etNewPassword.getText().toString().equals(etConfirmPassword.getText().toString())) {
            showToast("புதிய கடவுச்சொல் மற்றும் கடவுச்சொல் பொருந்தவில்லை");
        } else {
            String oldPassword = etOldPassword.getText().toString().trim();
            String newPassword = etNewPassword.getText().toString().trim();
            LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
            if (loginResponse != null) {
                String userId = loginResponse.getLoginDetails().get(0).getUserttype();
                showProgress();
                getPresenter().changePassword(userId, oldPassword, newPassword);
            }
        }
    }

    public void onGetChangePasswordResponseSuccess(HeaderResponse response) {
        hideProgress();
        showToast("உங்கள் கடவுச்சொல் வெற்றிகரமாக மாற்றப்பட்டது!");
        finish();
    }

    public void onFailure(String response) {
        hideProgress();
        showToast("பழைய கடவுச்சொல் தவறானது");
    }

    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }
}