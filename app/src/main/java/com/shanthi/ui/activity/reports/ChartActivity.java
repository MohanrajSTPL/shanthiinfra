package com.shanthi.ui.activity.reports;

        import android.os.Bundle;
        import android.util.Log;

        import com.shanthi.R;
        import com.shanthi.model.construction.Expense;
        import com.shanthi.model.order.OrderDetails;
        import com.shanthi.network.response.chart.ChartListResponse;
        import com.shanthi.network.response.onBoarding.LoginResponse;
        import com.shanthi.presenter.ChartPresenter.ChartDetailsPresenter;
        import com.shanthi.ui.base.BaseActivity;
        import com.github.mikephil.charting.charts.PieChart;
        import com.github.mikephil.charting.components.Description;
        import com.github.mikephil.charting.data.PieData;
        import com.github.mikephil.charting.data.PieDataSet;
        import com.github.mikephil.charting.data.PieEntry;
        import com.github.mikephil.charting.utils.ColorTemplate;

        import java.util.ArrayList;
        import java.util.List;

        import butterknife.BindView;
        import nucleus5.factory.RequiresPresenter;

        import static com.shanthi.constant.Constants.ACCOUNT_PREFS;
        import static com.shanthi.utils.AccountUtils.getLogin;

@RequiresPresenter(ChartDetailsPresenter.class)

public class ChartActivity extends BaseActivity<ChartDetailsPresenter> {

    @BindView(R.id.barChart)
    PieChart barChart;
   String sessionId,userType;
    private List<Expense> uomList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);

        showProgress();

        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
            sessionId = loginResponse.getLoginDetails().get(0).getSessionid();
            userType = loginResponse.getLoginDetails().get(0).getUserttype();
        }
        Expense expense = new Expense();
        expense.setSessionid(sessionId);
        expense.setUsertype(userType);

        getPresenter().getChartDetails(expense);
    }


    public void onChartDetailResponseSuccess(ChartListResponse response) {
        hideProgress();
        Log.d("response", String.valueOf(response.getExpense()));
        if(response.getExpense()!=null) {
          //  Integer amount = Integer.valueOf(response.getExpense().get(0).get());
//String  amount1 = response.getExpense().get(0).getExpenseName();
            List<PieEntry> barEntries = new ArrayList<>();
for(Expense expense: response.getExpense()) {
    Float amount = Float.parseFloat(expense.getAmount());
    String  amount1 = expense.getExpenseName();
    barEntries.add(new PieEntry(amount, amount1));
}
            PieDataSet barDataSet = new PieDataSet(barEntries, "ExpenseName");
            barDataSet.setColors(ColorTemplate.COLORFUL_COLORS);

            PieData barData = new PieData(barDataSet);
//            barData.setBarWidth(0.4f);
          //  barChart.animateY(5000);
            barChart.setData(barData);
            barChart.setDrawHoleEnabled(false);

         //   barChart.setFitBars(true);


            Description description = new Description();
            description.setText("Amount");
            barChart.setDescription(description);
            barChart.invalidate();
        }
    }



    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }
}
