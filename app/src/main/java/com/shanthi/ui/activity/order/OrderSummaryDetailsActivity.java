package com.shanthi.ui.activity.order;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.shanthi.R;
import com.shanthi.model.List.NewList;
import com.shanthi.model.order.ItemList;
import com.shanthi.model.order.OrderDetails;
import com.shanthi.model.order.OrderItem;
import com.shanthi.model.order.Uom;
import com.shanthi.network.response.List.NewListResponse;
import com.shanthi.network.response.order.SummaryDetailsListResponse;
import com.shanthi.presenter.order.OrderSummaryDetailsPresenter;
import com.shanthi.ui.adapter.order.OrderItemAdapter;
import com.shanthi.ui.adapter.order.OrderItemSummaryDetailsAdapter;
import com.shanthi.ui.adapter.order.OrderItemSummaryDetailsDriverAdapter;
import com.shanthi.ui.base.BaseActivity;
import com.shanthi.ui.fragment.dialogFragments.AlertDialogFragment;
import com.shanthi.utils.Interface_string;
import com.shanthi.utils.PrinterSpooler;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import nucleus5.factory.RequiresPresenter;

import static com.shanthi.ui.adapter.order.OrderItemSummaryDetailsDriverAdapter.*;
import static com.shanthi.utils.ConversionUtils.getJsonFromString;

@RequiresPresenter(OrderSummaryDetailsPresenter.class)

public class OrderSummaryDetailsActivity extends BaseActivity<OrderSummaryDetailsPresenter>
        implements  OrderItemSummaryDetailsAdapter.OrderItemDetailListener, OrderItemDriverDetailListener, Interface_string {

    @BindView(R.id.rvItems)
    RecyclerView rvItems;
    @BindView(R.id.rvDeliveryDetails)
    RecyclerView rvDeliveryDetails;
    @BindView(R.id.llCustomerName)
    LinearLayout llCustomerName;
    @BindView(R.id.llOrderId)
    LinearLayout llOrderId;
    @BindView(R.id.llDate)
    LinearLayout llDate;
    @BindView(R.id.llStatus)
    LinearLayout llStatus;
    @BindView(R.id.llAddress)
    LinearLayout llAddress;
    @BindView(R.id.llDeliveryDetails)
    LinearLayout llDeliveryDetails;
    @BindView(R.id.llItemDetails)
    LinearLayout llItemDetails;
    @BindView(R.id.llDeliveryDate)
    LinearLayout llDeliveryDate;
    @BindView(R.id.llDeliveryTime)
    LinearLayout llDeliveryTime;
    @BindView(R.id.llDispatchDate)
    LinearLayout llDispatchDate;
    @BindView(R.id.llDispatchTime)
    LinearLayout llDispatchTime;

    @BindView(R.id.tvId)
    TextView tvId;
    @BindView(R.id.tvCompanyName)
    TextView tvCompanyName;
    @BindView(R.id.tvProductName)
    TextView tvProductName;
    @BindView(R.id.tvQty)
    TextView tvQty;
    @BindView(R.id.tvInvoiceId)
    TextView tvInvoiceId;
    @BindView(R.id.tvInvoiceDate)
    TextView tvInvoiceDate;
    @BindView(R.id.tvVehNo)
    TextView tvVehNo;
    @BindView(R.id.tvEwayNo)
    TextView tvEwayNo;
    @BindView(R.id.tvTruck)
    TextView tvTruck;
    @BindView(R.id.tvDeliveryDate)
    TextView tvDeliveryDate;
    @BindView(R.id.tvDeliveryTime)
    TextView tvDeliveryTime;
    @BindView(R.id.tvDispatchDate)
    TextView tvDispatchDate;
    @BindView(R.id.tvDispatchTime)
    TextView tvDispatchTime;
    @BindView(R.id.print)
    Button print;
    @BindView(R.id.tvTransType)
    TextView tvTransType;
    @BindView(R.id.tvDeliveryAddress)
    TextView tvDeliveryAddress;
    @BindView(R.id.tvGoodsDelivered)
    TextView tvGoodsDelivered;
    @BindView(R.id.tvNoOfItems)
    TextView tvNoOfItems;


//    @BindView(R.id.tvSitenumber)
//    TextView tvSitenumber;
//
//    @BindView(R.id.tvPayment_type)
//    TextView tvPayment_type;

//    @BindView(R.id.print)
//    Button print;


    private List<OrderItem> orderItemList = new ArrayList<>();
    private List<OrderDetails> orderItemDetailsList = new ArrayList<>();
    private List<OrderDetails> orderItemDetailsList1 = new ArrayList<>();
    private List<Uom> uomList = new ArrayList<>();
    private OrderItemSummaryDetailsAdapter adapter;
    private OrderItemSummaryDetailsDriverAdapter deliveryAdapter;
    private OrderItemAdapter adapter1;
    private NewList order;
    String statusDone = "Done";
    String Screen = "null";
    String statusAwaitingDelivery = "Awaiting Delivery";
    String statusProcessingOrder = "Processing Order";
    String id = "1";
    String ListDate, StatusFlag = null;
    private ItemList orderItemDetails = null;
    String pay1 = "Credit";
    String pay2 = "Cash";
    String productName,quantity;


    public Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_summary_details);
        setToolbarTitle(getString(R.string.order_details));

// https://www.youtube.com/watch?v=Lt_VyD-b9_0

        setRecyclerView();

        ActivityCompat.requestPermissions(OrderSummaryDetailsActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PackageManager.PERMISSION_GRANTED);

        if (getIntent() != null && getIntent().getExtras() != null) {

            Screen = getIntent().getExtras().getString("screen");
            String orderStr = getIntent().getExtras().getString("order");
            ListDate = getIntent().getExtras().getString("Date");

            if (Screen != null && Screen.equalsIgnoreCase("outer_screen")) {
                order = getJsonFromString(orderStr, NewList.class);
            } else if (Screen != null && Screen.equalsIgnoreCase("inner_screen")) {
                order = getJsonFromString(orderStr, NewList.class);
            } else if (Screen != null && Screen.equalsIgnoreCase("push")) {
                order = getJsonFromString(orderStr, NewList.class);
            }

            if (order != null) {
                //  showProgress();
                /* To Display Product Details */
                getPresenter().getOrderDetails(order.getInvoiceId());

                /* To Display Summary Details */

                getPresenter().getDeliverySummaryDetails(order.getInvoiceId(),order.getTransType());

                tvId.setText(order.getId());
                if (order.getCompanyName() != null)
                    tvCompanyName.setText(order.getCompanyName());
                if (order.getProductName() != null)
                    tvProductName.setText(order.getProductName());
                if (order.getQuantity() != null)
                    tvQty.setText(order.getQuantity());
                if (order.getInvoiceId() != null)
                    tvInvoiceId.setText(order.getInvoiceId());
                if (order.getInvoiceDate() != null)
                    tvInvoiceDate.setText(order.getInvoiceDate());
                if (order.getVehiclenumber() != null && !(order.getVehiclenumber().isEmpty())) {
                    tvVehNo.setText(order.getVehiclenumber());
                } else {
                    tvVehNo.setText("Not Specified");
                }
                if (order.getEwaynumber() != null && !(order.getVehiclenumber().isEmpty())) {
                    tvEwayNo.setText(order.getEwaynumber());
                } else
                    {
                    tvEwayNo.setText("Not Specified");
                    }
                if (order.getTransType() != null && !(order.getTransType().isEmpty())) {
                    tvTransType.setText(order.getTransType());
                }
                else
                {
                    tvTransType.setText("Not Specified");
                }

                if (order.getDeliveryAddress() != null && !(order.getDeliveryAddress().isEmpty())) {
                    tvDeliveryAddress.setText(order.getDeliveryAddress());
                } else
                {
                    tvDeliveryAddress.setText("Not Specified");
                }

                if (order.getDeliveredGoods() != null && !(order.getDeliveredGoods().isEmpty())) {
                    tvGoodsDelivered.setText(order.getDeliveredGoods());
                }
                else
                {
                    tvGoodsDelivered.setText("Not Specified");
                }


                if (order.getNoOfItems() != null && !(order.getNoOfItems().isEmpty())) {
                    tvNoOfItems.setText(order.getNoOfItems());
                }
                else
                {
                    tvNoOfItems.setText("Not Specified");
                }
//                if (order.getDeliveryStatus() != null && order.getDeliveryStatus()==1) {
//                    tvNoOfItems.setText(order.getNoOfItems());
//                } else
//                {
//                    tvNoOfItems.setText("Not Specified");
//                }
            }
        }
    }



    public void OnClick(View view) {
        new PrinterSpooler().execute();

    }

    private void setRecyclerView() {
        adapter = new OrderItemSummaryDetailsAdapter();
        adapter.setListener(this);
        rvItems.setHasFixedSize(false);
        rvItems.setLayoutManager(new LinearLayoutManager(this));
        rvItems.setAdapter(adapter);

        deliveryAdapter = new OrderItemSummaryDetailsDriverAdapter();
        deliveryAdapter.setListener(this);
        rvDeliveryDetails.setHasFixedSize(false);
        rvDeliveryDetails.setLayoutManager(new LinearLayoutManager(this));
        rvDeliveryDetails.setAdapter(deliveryAdapter);
    }

    @OnClick(R.id.tvMovetoShip)
    public void onAddDriver() {
        //  showAddDriverPopUp();
    }



    public void onUpdateDeliveryStatusSucccess(SummaryDetailsListResponse response) {
        hideProgress();

        showToast("Delivered Sucessfully..!");

        getPresenter().getOrderDetails(order.getInvoiceId());
    }

    @Override
    public void onRestart() {
        super.onRestart();
    }

    /* Product Details */
    public void onNewOrderDetailsResponseSuccess(SummaryDetailsListResponse response) {
        hideProgress();

        try {
            adapter.setOrderItemDetailsList(response.getItem());
            Log.d("itemresponse", String.valueOf(response.getItem()));
            productName = response.getItem().get(0).getProductName();
            quantity = response.getItem().get(0).getQuantity();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("error", e.getMessage());
        }

        adapter.notifyDataSetChanged();
    }

/* Summary Details */

    public void onDeliverySummaryDetailsResponseSuccess(NewListResponse response) {
        hideProgress();
        try {
            deliveryAdapter.setOrderItemDetailsList(response.getOrderList());
         Log.d("refdcvbnm", String.valueOf(response.getOrderList()));
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("error", e.getMessage());
        }

        deliveryAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.tvOrderDelivered)
    public void onAddDelivery() {
        //   showAddDelivery();
    }


    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }

    public void onFailure(String error) {
        hideProgress();
        // rvItems.setVisibility(View.GONE);
        // showToast(error);
    }

//    @Override
//    public void onOrderItemDetailClicked(Item orderItemDetails) {
//
//    }

    @Override
    public void onOrderItemDriverDetailClicked(NewList orderItemDetails) {

    }

    @Override
    public void interface_connecting_string(int position, String check) {

    }

    @Override
    public void onOrderItemDetailClicked(ItemList orderItemDetails) {
        showProgress();
        this.orderItemDetails = orderItemDetails;
        AlertDialogFragment alertDialogFragment = AlertDialogFragment.newInstance("Confirm", "Want to Deliver Products?",
                "Yes", "No");
        alertDialogFragment.setListener(new AlertDialogFragment.AlertDialogListener() {
            @Override
            public void onYesClicked() {
                alertDialogFragment.dismiss();
                String itemId = orderItemDetails.getItemId();
                String salesId =orderItemDetails.getSalesId();

                getPresenter().updateDeliverStatus(salesId,itemId);
            }

            @Override
            public void onNoClicked() {
                alertDialogFragment.dismiss();
            }
        });

        alertDialogFragment.show(getSupportFragmentManager(), "AlertDialog");

    }

    @OnClick(R.id.print)
    public void print(View view){

        Toast.makeText(view.getContext(), "Processing...", Toast.LENGTH_SHORT).show();
        PdfDocument myPdfDocument = new PdfDocument();
        PdfDocument.PageInfo myPageInfo = new PdfDocument.PageInfo.Builder(300,600,1).create();
        PdfDocument.Page myPage = myPdfDocument.startPage(myPageInfo);

        Paint myPaint = new Paint();
        String myString = "         BMS TRADERS - SARAVANAMPATTI  " +"\n"+
                "                         9952300009  " +"\n"+
                "Invoice Id  :"+order.getInvoiceId()+"\n"+
                "InvoiceDate :"  +order.getInvoiceDate() + "\n"+
                "Customer Name :"  + order.getCompanyName() + "\n"+
                "Transaction Type :" + order.getTransType() + "\n" +
                "Delivery Address :" + order.getDeliveryAddress() + "\n" +
                "ProductName :"  + productName + "\n"+
                "Quantity :" + quantity + "\n"+
                "Vehiclenumber :"  +order.getVehiclenumber() ;



        int x = 10, y=25;

        //new line
        for (String line:myString.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            y+=myPaint.descent()-myPaint.ascent();
        }

        myPdfDocument.finishPage(myPage);

        String myFilePath = Environment.getExternalStorageDirectory().getPath() + "/test1_pdf.pdf";
        File myFile = new File(myFilePath);
        try {
            myPdfDocument.writeTo(new FileOutputStream(myFile));
        }
        catch (Exception e){
            e.printStackTrace();
        }

        myPdfDocument.close();
        open_File();
    }

    public void open_File(){

        File file = new File(Environment.getExternalStorageDirectory().toString()+"/test1_pdf.pdf");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file), "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        Intent intent1 = Intent.createChooser(intent, "Open With");
        startActivity(intent1);
//        try {
//            getContext().startActivity(intent1);
//        } catch (ActivityNotFoundException e) {
//            // Instruct the user to install a PDF reader here, or something
//        }

    }

    protected Context getContext() {
        return this.context;
    }

}