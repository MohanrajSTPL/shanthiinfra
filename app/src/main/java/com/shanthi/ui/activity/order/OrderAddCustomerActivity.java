package com.shanthi.ui.activity.order;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.shanthi.R;
import com.shanthi.model.order.OrderDetails;
import com.shanthi.network.response.HeaderResponse;
import com.shanthi.presenter.order.OrderAddCustomerPresenter;
import com.shanthi.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;
import nucleus5.factory.RequiresPresenter;

@RequiresPresenter(OrderAddCustomerPresenter.class)
public class OrderAddCustomerActivity extends BaseActivity<OrderAddCustomerPresenter> {
    @BindView(R.id.llCompany)
    LinearLayout llCompany;
    @BindView(R.id.llIndividual)
    LinearLayout llIndividual;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.rbCompany)
    RadioButton rbCompany;
    @BindView(R.id.rbIndividual)
    RadioButton rbIndividual;
    @BindView(R.id.etCompanyName)
    EditText etCompanyName;
    @BindView(R.id.etInchargeName)
    EditText etInchargeName;
    @BindView(R.id.etInchargeDesignation)
    EditText etInchargeDesignation;
    @BindView(R.id.etInchargeMobileNumber)
    EditText etInchargeMobileNumber;
    @BindView(R.id.etCompanyMobileNumber)
    EditText etCompanyMobileNumber;
    @BindView(R.id.etBranch)
    EditText etBranch;
    @BindView(R.id.etAddress)
    EditText etAddress;
    @BindView(R.id.etCity)
    EditText etCity;
    @BindView(R.id.etState)
    EditText etState;
    @BindView(R.id.etPincode)
    EditText etPincode;
    @BindView(R.id.etGST)
    EditText etGST;
    @BindView(R.id.etPAN)
    EditText etPAN;

    // individual
    @BindView(R.id.etCustomerName)
    EditText etCustomerName;
    @BindView(R.id.etCustomerMobileNumber)
    EditText etCustomerMobileNumber;
    @BindView(R.id.etCustomerAddress)
    EditText etCustomerAddress;
    @BindView(R.id.etCustomerCity)
    EditText etCustomerCity;
    @BindView(R.id.etCustomerState)
    EditText etCustomerState;
    @BindView(R.id.etCustomerPincode)
    EditText etCustomerPincode;
    @BindView(R.id.etCustomerPAN)
    EditText etCustomerPAN;
    private boolean isCompany = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_customer);
        setToolbarTitle(getString(R.string.create_new_customer));
        setRadioButtonClickListener();
    }

    private void setRadioButtonClickListener() {
        showToast("radiobutton");
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                RadioButton rb = (RadioButton) radioGroup.findViewById(checkedId);
                if (rb.getText().toString().equalsIgnoreCase(getString(R.string.company))) {
                    llIndividual.setVisibility(View.GONE);
                    llCompany.setVisibility(View.VISIBLE);
                    isCompany = true;
                } else if (rb.getText().toString().equalsIgnoreCase(getString(R.string.individual_customer))) {
                    llCompany.setVisibility(View.GONE);
                    llIndividual.setVisibility(View.VISIBLE);
                    isCompany = false;
                }
            }
        });
    }
    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }
    @OnClick(R.id.tvSubmit)
    public void onSubmitClicked() {

        if (isCompany && etCompanyName.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_company_name));
            return;
        }
//        if (isCompany && etInchargeName.getText().toString().trim().isEmpty()) {
//            showToast(getString(R.string.enter_incharge_name));
//            return;
//        }
        if (isCompany && etInchargeDesignation.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_incharge_designation));
            return;
        }
        if (isCompany && etInchargeMobileNumber.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_incharge_mobile_number));
            return;
        }
        /*if (isCompany && etCompanyMobileNumber.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_company_mobile_number));
            return;
        }
        if (isCompany && etBranch.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_branch));
            return;
        }
        if (isCompany && etAddress.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_address));
            return;
        }*/
        if (!etCity.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_city));
            return;
        }
        /*if (isCompany && etState.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_state));
            return;
        }
        if (isCompany && etPincode.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_pincode));
            return;
        }
        if (isCompany && etGST.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_gst));
            return;
        }
        if (isCompany && etPAN.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_pan));
            return;
        }*/

        // individual
        if ( !etCustomerName.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_customer_name));
            return;
        }
        if (!etCustomerMobileNumber.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_mobile_number));
            return;
        }
        /*if (!isCompany && etCustomerAddress.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_address));
            return;
        }*/
        if (!isCompany && etCustomerCity.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_city));
            return;
        }
        /*if (!isCompany && etCustomerState.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_state));
            return;
        }
        if (!isCompany && etCustomerPincode.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_pincode));
            return;
        }

        if (!isCompany && etCustomerPAN.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_pan));
            return;
        }*/

        // company
        String companyName = etCompanyName.getText().toString().trim();
        String inchargeName = etInchargeName.getText().toString().trim();
        String inchargeDestination = etInchargeDesignation.getText().toString().trim();
        String inchargeMobileNumber = etInchargeMobileNumber.getText().toString().trim();
        String companyMobileNumber = etCompanyMobileNumber.getText().toString().trim();
        String branch = etBranch.getText().toString().trim();
        String address = etAddress.getText().toString().trim();
        String city = etCity.getText().toString().trim();
        String state = etState.getText().toString().trim();
        String pinCode = etPincode.getText().toString().trim();
        String gst = etGST.getText().toString().trim();
        String pan = etPAN.getText().toString().trim();

        // individual
        String customerName = etCustomerName.getText().toString().trim();
        String customerMobileNumber = etCustomerMobileNumber.getText().toString().trim();
        String customerAddress = etCustomerAddress.getText().toString().trim();
        String customerCity = etCustomerCity.getText().toString().trim();
        String customerState = etCustomerState.getText().toString().trim();
        String customerPincode = etCustomerPincode.getText().toString().trim();
        String customerPAN = etCustomerPAN.getText().toString().trim();

       showProgress();
//        getPresenter().addCustomerCompanyDetails(isCompany, companyName, inchargeName, inchargeDestination, inchargeMobileNumber,
//                companyMobileNumber, branch, address, city, state, pinCode, gst, pan, customerName, customerMobileNumber,
//                customerAddress, customerCity, customerState, customerPincode, customerPAN);

       /* Intent intent = new Intent(this, OrderEnquiryMultiProductDetailsActivity.class);

        // customer type
        intent.putExtra("isCompany", isCompany);
        // company details
        intent.putExtra("companyName", companyName);
        intent.putExtra("inchargeName", inchargeName);
        intent.putExtra("inchargeDestination", inchargeDestination);
        intent.putExtra("inchargeMobileNumber", inchargeMobileNumber);
        intent.putExtra("companyMobileNumber", companyMobileNumber);
        intent.putExtra("branch", branch);
        intent.putExtra("address", address);
        intent.putExtra("city", city);
        intent.putExtra("state", state);
        intent.putExtra("pinCode", pinCode);
        intent.putExtra("gst", gst);
        intent.putExtra("pan", pan);

        // individual details
        intent.putExtra("customerName", customerName);
        intent.putExtra("customerMobileNumber", customerMobileNumber);
        intent.putExtra("customerAddress", customerAddress);
        intent.putExtra("customerCity", customerCity);
        intent.putExtra("customerState", customerState);
        intent.putExtra("customerPincode", customerPincode);
        intent.putExtra("customerPAN", customerPAN);

        // start intent
        startActivity(intent);*/
    }

    public void onAddCustomerCompanyDetailsResponseSuccess(HeaderResponse response) {
        hideKeyBoard();
        hideProgress();
        changeActivity(OrderActivity.class);
        finish();
    }
}