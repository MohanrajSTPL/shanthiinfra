package com.shanthi.ui.activity.order;

import android.os.Bundle;

import com.shanthi.R;
import com.shanthi.model.order.OrderDetails;
import com.shanthi.ui.base.BaseActivity;

import butterknife.OnClick;

public class OrderSelectedActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_selected);
        setToolbarTitle(getString(R.string.order));
    }

    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }

    @OnClick(R.id.tvCreateNewCustomer)
    public void onCreateNewCustomer() {
        changeActivity(OrderAddCustomerActivity.class);
    }

    @OnClick(R.id.tvNewOrder)
    public void onNewOrder() {
        changeActivity(OrderActivity.class);
    }
}
