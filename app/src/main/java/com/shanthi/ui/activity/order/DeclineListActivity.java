package com.shanthi.ui.activity.order;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.shanthi.R;
import com.shanthi.model.construction.Expense;
import com.shanthi.model.order.OrderDetails;
import com.shanthi.network.response.expenseType.ApprovalResponse;
import com.shanthi.network.response.expenseType.UserExpenseResponse;
import com.shanthi.network.response.onBoarding.LoginResponse;
import com.shanthi.presenter.decline.DeclineListPresenter;
import com.shanthi.presenter.expense.ExpenseListPresenter;
import com.shanthi.ui.adapter.decline.DeclineListAdapter;
import com.shanthi.ui.adapter.expense.ExpenseListAdapter;
import com.shanthi.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import nucleus5.factory.RequiresPresenter;

import static com.shanthi.constant.Constants.ACCOUNT_PREFS;
import static com.shanthi.utils.AccountUtils.getLogin;
import static com.shanthi.constant.Constants.ACCOUNT_PREFS;

@RequiresPresenter(DeclineListPresenter.class)

public class DeclineListActivity extends BaseActivity <DeclineListPresenter> implements DeclineListAdapter.OrderListener {
    private DeclineListAdapter adapter;
    @BindView(R.id.rvOrder)
    RecyclerView rvOrder;
    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.tvNoData)
    TextView tvNoData;
    String operation ="delete";
    String sessionId = null;
    private List<Expense> orderList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decline_list);
         setToolbarTitle("Decline List");
        setRecyclerView();
        //   setSearchEditTextListener();

        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
            sessionId = loginResponse.getLoginDetails().get(0).getSessionid();
        }

        Expense expense = new Expense();
        expense.setSessionid(sessionId);
        getPresenter().getMyDeclineList(expense);
    }


    private void setRecyclerView() {
        adapter = new DeclineListAdapter();
        adapter.setListener(this);
        rvOrder.setHasFixedSize(false);
        rvOrder.setLayoutManager(new LinearLayoutManager(this));
        rvOrder.setAdapter(adapter);
    }
    public void onUserExpenseListResponseSuccess(UserExpenseResponse response) {
        hideProgress();
        //tvNoData.setVisibility(View.GONE);

        if (response.getExpense() != null) {
            orderList = response.getExpense();
            adapter.setOrderList(response.getExpense());
            adapter.notifyDataSetChanged();
            rvOrder.setVisibility(View.VISIBLE);
        }
        else if(response.getMessage().equals("No Expense found")){
            rvOrder.setVisibility(View.GONE);
            etSearch.setVisibility(View.GONE);
        }
//        else{
//            rvOrder.setVisibility(View.GONE);
//            etSearch.setVisibility(View.GONE);
//        }
    }

    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }


    @Override
    public void onOrderClicked(Expense order) {

    }

    @Override
    public void onOrderRemoveClicked(Expense order) {
       // getPresenter().sendApproval(order.getExpenseID(),operation);
    }

    @Override
    public void onDelete(Expense order) {
        getPresenter().deleteExpense(order.getExpenseId(),operation);

    }

    public void onDeleteResponseSuccess(ApprovalResponse response) {
        hideProgress();
        showToast("Expense Deleted..");
        if(response!=null) {
            Expense expense = new Expense();
            expense.setSessionid(sessionId);
            getPresenter().getMyDeclineList(expense);
        }
        else{
            tvNoData.setVisibility(View.GONE);
        }
        //  tvNoData.setVisibility(View.GONE);


    }
}