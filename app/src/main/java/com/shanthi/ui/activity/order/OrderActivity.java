package com.shanthi.ui.activity.order;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.shanthi.R;
import com.shanthi.model.order.OrderDetails;
import com.shanthi.model.shanthiInfra.Contractor;
import com.shanthi.model.shanthiInfra.Sites;
import com.shanthi.model.shanthiInfra.Vendor;
import com.shanthi.model.shanthiInfra.Work;
import com.shanthi.network.response.expenseType.AddExpenseResponse;
import com.shanthi.network.response.expenseType.ContractorResponse;
import com.shanthi.network.response.onBoarding.LoginResponse;
import com.shanthi.presenter.order.OrderPresenter;
import com.shanthi.ui.activity.home.HomeActivity;
import com.shanthi.ui.adapter.order.UomSpinnerAdapter;
import com.shanthi.ui.adapter.order.WorkerAdapter;
import com.shanthi.ui.base.BaseActivity;
import com.shanthi.utils.AppUtils;
import com.shanthi.utils.TimeUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import nucleus5.factory.RequiresPresenter;

import static com.shanthi.constant.Constants.ACCOUNT_PREFS;
import static com.shanthi.utils.AccountUtils.getLogin;

@RequiresPresenter(OrderPresenter.class)

public class OrderActivity extends BaseActivity<OrderPresenter>  {

    @BindView(R.id.llCustomer)
    LinearLayout llCustomer;
    @BindView(R.id.spCustomer)
    Spinner spCustomer;
    @BindView(R.id.etOrderDate)
    TextView etOrderDate;
    @BindView(R.id.tvSubmit)
    TextView tvSubmit;
    @BindView(R.id.etAmount)
    EditText etAmount;
    @BindView(R.id.etDescription)
    EditText etDescription;
    @BindView(R.id.spExpensesType)
    Spinner spExpensesType;
    @BindView(R.id.spJob)
    Spinner spJob;
//    @BindView(R.id.etExpensesType)
//    EditText etExpensesType;
//    @BindView(R.id.spStatus)
//    Spinner spStatus;
    private List<Vendor> expenses = new ArrayList<>();
    private List<Vendor> uomList = new ArrayList<>();
    private List<Sites> sites = new ArrayList<>();
    private List<Sites> siteList = new ArrayList<>();
    private List<Work> works = new ArrayList<>();
    private List<Work> workList = new ArrayList<>();
    private boolean isCustomer = true;
    private int year, month, day;
    private int hour,minute,second ,date;
    String siteId,siteName,workName,workId,custId,vendorName, vendorId,customerName,userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        setToolbarTitle(" Add Contractor Payment");
        String type = "ETYPE";
        getPresenter().getVendor();
        getPresenter().getSite();
        getPresenter().getJob();
        initDate();
    }

    private void initDate() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        String date = year + "-" + AppUtils.getMonthHrMinStr((month + 1)) + "-" + AppUtils.getMonthHrMinStr(day);
      //  String viewdate = AppUtils.getMonthHrMinStr(day) + "-" + AppUtils.getMonthHrMinStr((month + 1)) + "-" + year;

        etOrderDate.setText(date);
    }

    @OnClick(R.id.etOrderDate)
    public void onFromDateClicked() {
        DatePickerDialog dialog = new DatePickerDialog(this, R.style.CustomDatePicker,
                (datePicker, year1, month1, day1) -> {
                  String   date = year1 + "-" + AppUtils.getMonthHrMinStr((month1 + 1)) + "-" + AppUtils.getMonthHrMinStr(day1);
             //       String viewdate1 = AppUtils.getMonthHrMinStr(day1) + "-" + AppUtils.getMonthHrMinStr((month1 + 1)) + "-" + year1;
                    etOrderDate.setText(date);
                }, year, month, day);
        dialog.getDatePicker().setMaxDate(TimeUtils.getCurrentTimeInMs() - 10000);
        dialog.show();
    }

    @OnClick(R.id.tvSubmit)
    public void onSubmit() {
        if (siteId == null) {
            showToast("Select Site");
            return;
        }
        if (vendorId == null) {
            showToast("Select Contractor Name");
            return;
        }

        if (workId == null) {
            showToast("Select Work Order");
            return;
        }

        if (etAmount.getText().toString().trim().isEmpty()) {
            showToast("Enter Amount");
            return;
        }

        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
            userId = loginResponse.getLoginDetails().get(0).getSessionid();
        }
        tvSubmit.setClickable(false);
        Contractor newProduct = new Contractor();
        newProduct.setSiteId(siteId);
        newProduct.setVendorid(vendorId);
        newProduct.setworkid(workId);
        newProduct.setAmount(etAmount.getText().toString());
        newProduct.setTdate(etOrderDate.getText().toString());
        newProduct.setDescription(etDescription.getText().toString());
        newProduct.setSessionid(userId);
        showProgress();
        Log.d("productdetails", String.valueOf(newProduct));
        getPresenter().addContractor(newProduct);
//        showToast("success");
//        Intent i = new Intent(this,HomeActivity.class);
//        startActivity(i);
    }

    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }

    public void onAddExpenseResponseSuccess(AddExpenseResponse response) {
        showToast("Successfully Added");
        Intent intent = new Intent(this,HomeActivity.class);
        startActivity(intent);
    }

    public void onExpenseTypeSuccess(ContractorResponse response) {
        expenses = response.getVendor();
        Vendor orderItemDummy = new Vendor();
        orderItemDummy.setVendorName("Select Contractor Name");
        uomList.add(orderItemDummy);
        uomList.addAll(response.getVendor());

        ArrayList<String> customerListArray = new ArrayList<>();
        for (Vendor customers : uomList) {
            customerListArray.add(customers.getVendorName());
        }

        UomSpinnerAdapter uomSpinnerAdapter = new UomSpinnerAdapter(this,
                R.layout.spinner_list_item, customerListArray);
        spCustomer.setAdapter(uomSpinnerAdapter);
        spCustomer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {

                if (position != 0) {
                    String customerLists = uomSpinnerAdapter.getItem(position);
                    vendorName = customerLists;
                    vendorId = uomList.get(position).getVendorId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });
    }

    public void onSiteResponseSuccess(ContractorResponse response) {
        sites = response.getSites();

        Sites orderItemDummy = new Sites();
        orderItemDummy.setSiteName("Select Site");
        siteList.add(orderItemDummy);
        siteList.addAll(response.getSites());

        ArrayList<String> customerListArray = new ArrayList<>();
        for (Sites customers : siteList) {
            customerListArray.add(customers.getSiteName());
        }

        UomSpinnerAdapter uomSpinnerAdapter = new UomSpinnerAdapter(this,
                                   R.layout.spinner_list_item, customerListArray);
        spExpensesType.setAdapter(uomSpinnerAdapter);
        spExpensesType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {

                if (position != 0) {

                    String customerLists = uomSpinnerAdapter.getItem(position);
                    siteName = customerLists;
                    siteId = siteList.get(position).getSiteId();
                    custId = siteList.get(position).getCustomerId();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });
    }

    public void onJobResponseSuccess(ContractorResponse response) {
        works = response.getWorks();

        Work orderItemDummy = new Work();
        orderItemDummy.setWorkname("Select Work Order");
        workList.add(orderItemDummy);
        workList.addAll(response.getWorks());

        ArrayList<String> customerListArray = new ArrayList<>();
        for (Work customers : workList) {
            customerListArray.add(customers.getWorkname());
        }

        WorkerAdapter uomSpinnerAdapter = new WorkerAdapter(this,
                                   R.layout.spinner_list_item, customerListArray);
        spJob.setAdapter(uomSpinnerAdapter);
        spJob.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {

                if (position != 0) {

                    String customerLists = uomSpinnerAdapter.getItem(position);
                    workName = customerLists;
                    workId = workList.get(position).getWorkid();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });
    }

}
