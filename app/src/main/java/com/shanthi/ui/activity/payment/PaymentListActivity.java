package com.shanthi.ui.activity.payment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.shanthi.R;
import com.shanthi.model.construction.Expense;
import com.shanthi.model.order.OrderDetails;
import com.shanthi.model.payment.Payments;
import com.shanthi.network.response.payment.PaymentListResponse;
import com.shanthi.presenter.payment.PaymentListPresenter;
import com.shanthi.ui.adapter.payment.PaymentListAdapter;
import com.shanthi.ui.base.BaseActivity;
import com.shanthi.ui.fragment.shanthiInfra.ExpenseListFragment;
import com.shanthi.ui.fragment.shanthiInfra.PaymentListFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import nucleus5.factory.RequiresPresenter;

import static com.shanthi.utils.ConversionUtils.getStringFromJson;

@RequiresPresenter(PaymentListPresenter.class)

public class PaymentListActivity extends BaseActivity<PaymentListPresenter> implements PaymentListAdapter.OrderListener{

    @BindView(R.id.rvOrder)
    RecyclerView rvOrder;
    @BindView(R.id.etSearch)
    EditText etSearch;
    private PaymentListAdapter adapter;
    private List<Payments> orderList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_list_ativity);
        setRecyclerView();
        setSearchEditTextListener();
        setToolbarTitle("Payment List");
        getPresenter().getMyPaymentList();
    }
    private void setSearchEditTextListener() {
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filter(editable.toString());
               /* showProgress();
                getPresenter().searchFollowUp(editable.toString());*/
            }
        });
    }

    private void filter(String text) {
        List<Payments> filteredList = new ArrayList<>();
        // Looping through existing elements

        for (Payments item : orderList) {

            if ( item.getSite() != null && item.getSite().toLowerCase().contains(text.toLowerCase())
                    || item.getMode() != null && item.getMode().toLowerCase().contains(text.toLowerCase())
                    || item.getDate() != null && item.getDate().toLowerCase().contains(text.toLowerCase())) {
                // Adding the element to filtered list
                filteredList.add(item);
            }
        }
        //calling a method of the adapter class and passing the filtered list
        adapter.filterList(filteredList);
    }
    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }

    private void setRecyclerView() {
        adapter = new PaymentListAdapter();
        adapter.setListener(this);
        rvOrder.setHasFixedSize(false);
        rvOrder.setLayoutManager(new LinearLayoutManager(this));
        rvOrder.setAdapter(adapter);
    }

    public void onPaymentListResponseSuccess(PaymentListResponse response) {
        hideProgress();
        //tvNoData.setVisibility(View.GONE);
        rvOrder.setVisibility(View.VISIBLE);
        if (response.getPayments() != null)
            orderList = response.getPayments();
        adapter.setOrderList(response.getPayments());
        adapter.notifyDataSetChanged();
    }
    @Override
    public void onOrderClicked(Payments order) {


        String orderStr = getStringFromJson(order);
        PaymentListFragment orderItemFragment = PaymentListFragment.newInstance();
        orderItemFragment.setOrderItemList(orderList);

        Bundle b = new Bundle();
        b.putString("Expense",orderStr);
        orderItemFragment.setArguments(b);
        orderItemFragment.show(getSupportFragmentManager(), "");

    }



    @Override
    public void onOrderRemoveClicked(Payments order) {

    }
}
