package com.shanthi.ui.activity.payment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.shanthi.R;
import com.shanthi.model.order.OrderDetails;
import com.shanthi.model.payment.AddPayment;
import com.shanthi.model.payment.PaymentType;
import com.shanthi.model.shanthiInfra.Sites;
import com.shanthi.network.response.expenseType.AddExpenseResponse;
import com.shanthi.network.response.expenseType.ContractorResponse;
import com.shanthi.network.response.onBoarding.LoginResponse;
import com.shanthi.network.response.payment.AddPaymentResponse;
import com.shanthi.presenter.payment.PaymentPresenter;
import com.shanthi.ui.activity.home.HomeActivity;
import com.shanthi.ui.adapter.order.UomSpinnerAdapter;
import com.shanthi.ui.base.BaseActivity;
import com.shanthi.utils.AppUtils;
import com.shanthi.utils.TimeUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import nucleus5.factory.RequiresPresenter;

import static com.shanthi.constant.Constants.ACCOUNT_PREFS;
import static com.shanthi.utils.AccountUtils.getLogin;
@RequiresPresenter(PaymentPresenter.class)

public class PaymentActivity  extends BaseActivity <PaymentPresenter>{

    @BindView(R.id.spCustomer)
    Spinner spCustomer;
    @BindView(R.id.etOrderDate)
    TextView etOrderDate;
    @BindView(R.id.tvSubmit)
    TextView tvSubmit;
    @BindView(R.id.etAmount)
    EditText etAmount;
    @BindView(R.id.etDescription)
    EditText etDescription;
    @BindView(R.id.spExpensesType)
    Spinner spExpensesType;

    String userId,siteId,vendorId,workId,expId,siteName,paymentType,paymentId;
    private List<PaymentType> paymentTypes = new ArrayList<>();
    private List<PaymentType> paymentTypeList = new ArrayList<>();
    private List<Sites> sites = new ArrayList<>();
    private List<Sites> siteList = new ArrayList<>();
    private int year, month, day;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        setToolbarTitle("Payment Received");
        getPresenter().getPayment();
        getPresenter().getSite();
        initDate();
    }

    private void initDate() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
     //   String date = AppUtils.getMonthHrMinStr(day) + "-" + AppUtils.getMonthHrMinStr((month + 1)) + "-" + year;

         String date = year + "-" + AppUtils.getMonthHrMinStr((month + 1)) + "-" + AppUtils.getMonthHrMinStr(day);
        etOrderDate.setText(date);
    }
    @OnClick(R.id.etOrderDate)
    public void onFromDateClicked() {
        DatePickerDialog dialog = new DatePickerDialog(this, R.style.CustomDatePicker,
                (datePicker, year1, month1, day1) -> {
                    String date = year1 + "-" + AppUtils.getMonthHrMinStr((month1 + 1)) + "-" + AppUtils.getMonthHrMinStr(day1);
                    //   String date = AppUtils.getMonthHrMinStr(day1) + "-" + AppUtils.getMonthHrMinStr((month1 + 1)) + "-" + year1;
                    etOrderDate.setText(date);
                }, year, month, day);
        dialog.getDatePicker().setMaxDate(TimeUtils.getCurrentTimeInMs() - 10000);
        dialog.show();
    }
    @OnClick(R.id.tvSubmit)
    public void onSubmit() {

        if (siteId == null) {
            showToast("Select Site");
            return;
        }
        if (paymentId == null) {
            showToast("Select PaymentType");
            return;
        }
        if (etAmount.getText().toString().trim().isEmpty()) {
            showToast("Enter Amount");
            return;
        }

        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
            userId = loginResponse.getLoginDetails().get(0).getSessionid();
        }
       tvSubmit.setClickable(false);

        AddPayment newProduct = new AddPayment();
        newProduct.setSiteId(siteId);
        newProduct.setPid(paymentId);
        newProduct.setPtype(paymentType);
        newProduct.setAmount(etAmount.getText().toString());
        newProduct.setTdate(etOrderDate.getText().toString());
        newProduct.setDescription(etDescription.getText().toString());
        newProduct.setSessionid(userId);
        showProgress();
        Log.d("productdetails", String.valueOf(newProduct));
        getPresenter().addContractor(newProduct);

    }

    public void onAddPaymentResponseSuccess(AddExpenseResponse response) {
        showToast("Successfully Added");
        Intent intent = new Intent(this,HomeActivity.class);
        startActivity(intent);
    }
    public void onSiteResponseSuccess(ContractorResponse response) {

        sites = response.getSites();
        Sites orderItemDummy = new Sites();
        orderItemDummy.setSiteName("Select Site");
        siteList.add(orderItemDummy);
        siteList.addAll(response.getSites());

        ArrayList<String> customerListArray = new ArrayList<>();
        for (Sites customers : siteList) {
            customerListArray.add(customers.getSiteName());
        }

        UomSpinnerAdapter uomSpinnerAdapter = new UomSpinnerAdapter(this,
                R.layout.spinner_list_item, customerListArray);
        spCustomer.setAdapter(uomSpinnerAdapter);
        spCustomer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {

                if (position != 0) {
                    String customerLists = uomSpinnerAdapter.getItem(position);
                    siteName = customerLists;
                    siteId = siteList.get(position).getSiteId();
                   // custId = siteList.get(position).getCustomerId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });
    }

    public void onPaymentResponseSuccess(AddPaymentResponse response) {
        paymentTypes = response.getPaymenttype();

        PaymentType orderItemDummy = new PaymentType();
        orderItemDummy.setType("Select PaymentType");
        paymentTypeList.add(orderItemDummy);
        paymentTypeList.addAll(response.getPaymenttype());

        ArrayList<String> customerListArray = new ArrayList<>();
        for (PaymentType customers : paymentTypeList) {
            customerListArray.add(customers.getType());
        }

        UomSpinnerAdapter uomSpinnerAdapter = new UomSpinnerAdapter(this,
                R.layout.spinner_list_item, customerListArray);
        spExpensesType.setAdapter(uomSpinnerAdapter);
        spExpensesType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {

                if (position != 0) {
                    String customerLists = uomSpinnerAdapter.getItem(position);
                    paymentType = customerLists;
                    paymentId = siteList.get(position).getSiteId();
                    // custId = siteList.get(position).getCustomerId();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });
    }


    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }
}
