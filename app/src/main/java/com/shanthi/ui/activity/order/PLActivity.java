package com.shanthi.ui.activity.order;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.shanthi.R;
import com.shanthi.model.order.OrderDetails;
import com.shanthi.model.shanthiInfra.PL;
import com.shanthi.network.response.PLChart.PLChartResponse;
import com.shanthi.network.response.onBoarding.LoginResponse;
import com.shanthi.presenter.PL.PLPresenter;

import com.shanthi.ui.adapter.PL.PLAdapter;

import com.shanthi.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import nucleus5.factory.RequiresPresenter;

import static com.shanthi.constant.Constants.ACCOUNT_PREFS;
import static com.shanthi.utils.AccountUtils.getLogin;

@RequiresPresenter(PLPresenter.class)

public class PLActivity extends BaseActivity<PLPresenter> implements PLAdapter.OrderListener {
    @BindView(R.id.rvOrder)
    RecyclerView rvOrder;
    private PLAdapter adapter;
    private int year, month, day;
    String status = "Done";
    String noOfItems,deliveredGoods;
    private String orderDate,ListDate;
    private String viewstate = "PENDING";
    String date;

    private List<PL> orderList = new ArrayList<>();
    String state ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pl);
        setToolbarTitle("PL List");
        setRecyclerView();
        String sessionId = null;
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
            sessionId = loginResponse.getLoginDetails().get(0).getSessionid();
        }

        getPresenter().getPLList();
    }


    private void setRecyclerView() {
        adapter = new PLAdapter();
        adapter.setListener(this);
        rvOrder.setHasFixedSize(false);
        rvOrder.setLayoutManager(new LinearLayoutManager(this));
        rvOrder.setAdapter(adapter);
    }

    public void onPLResponseSuccess(PLChartResponse response) {
        hideProgress();

        //tvNoData.setVisibility(View.GONE);
        rvOrder.setVisibility(View.VISIBLE);
        if (response.getPL() != null)
            orderList = response.getPL();
        adapter.setOrderList(response.getPL());
        adapter.notifyDataSetChanged();
    }
    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }



    @Override
    public void onOrderClicked(PL order) {

    }

    @Override
    public void onOrderRemoveClicked(PL order) {

    }
}
